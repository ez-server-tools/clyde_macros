#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|======

#Event cEoNMaker	"cEoN"

|==== Sub Event_cEoNMaker
	Sub Event_cEoNMaker
		/call cEoNMakerLogic
	/return
|==== End Sub Event_cEoNMaker

|Have an empty Magic Box in slot 10
Sub cEoNMakerLogic
|This is the slot of the Magic Box I'll be using. Use 10 since I'm using Auto Inventory
	/declare MagicBoxSlot string local pack10
	|These are the items I'm going to check for, grab 2 of, and combine them in
	/declare item1  string local Ore From North Qeynos
	/declare item2  string local Ore From Butcherblock Mountains
	/declare item3  string local Ore From Lesser Faydark
	/declare item4  string local Ore From Rathe Mountains
	/declare item5  string local Ore From Oasis of Marr
	/declare item6  string local Ore From Skyfire Mountains
	/declare item7  string local Ore From The Hole
	/declare item8  string local Ore From Kedge Keep
	/declare item9  string local Ore From Timorous Deep
	/declare item10 string local Ore From The Halls of Betrayal
	/if (!${Defined[BagNum]}) /declare BagNum int local
	/if (!${Defined[BagSlot]}) /declare BagSlot int local
	/declare i int local 1

	/keypress OPEN_INV_BAGS
	|Delay 1minute until that condition is met
	/delay 1m ${Window[pack8].Open}

	:Repeat
	|Cycle through all 7 of the items above
	/for i 1 to 10			
		/declare j int local 1

		/for j 1 to 10 {
			/if (!${FindItem[=${item${j}}].ID}) {
				/echo You don't have all ores, exiting!
				/return
			}
		}
		
		|Pickup the first one
		/varcalc BagNum ${FindItem[=${item${i}}].ItemSlot}
		/varcalc BagNum ${BagNum}-22	
		/varcalc BagSlot ${FindItem[=${item${i}}].ItemSlot2}
		/varcalc BagSlot ${BagSlot}+1
		
		
		/ctrlkey /itemnotify in pack${BagNum} ${BagSlot} leftmouseup
		/delay 1m ${Cursor.ID} == ${FindItem[=${item${i}}].ID}
		/echo Item: ${item${i}}
		/itemnotify in ${MagicBoxSlot} ${i} leftmouseup
		/delay 1m !${Cursor.ID}
		/if (${i}==10) {
			|Hit Combine and put it away
			/combine ${MagicBoxSlot}
			/delay 1m ${Cursor.ID}
			/delay 2
			/autoinventory
			/delay 1m !${Cursor.ID}	
		}
		/echo I: ${i}
		/next i

		/for j 1 to 10 {
			/if (!${FindItem[=${item${j}}].ID}) {
				/goto :Done
			}
		}
		/goto :Repeat
		
		:Done
		/echo Done!
/return