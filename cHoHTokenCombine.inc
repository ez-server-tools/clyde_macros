#warning


|====== Modified by Clyde for EZ Server to fit my cMacro echo system
|====== Original credit: Knorgar - http://ezserver.online/forums/index.php?topic=5167.15
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|======
|112921 - 112927 - T3 IDs
|112931 - 112937 - T4 IDs

#Event cHoHTokenCombine	"cHoHTok"

|==== Sub Event_cHoHTokenCombine
	Sub Event_cHoHTokenCombine
		/call cHoHTokenCombineLogic
	/return
|==== End Sub Event_cHoHTokenCombine

|Combine HoH tokens
|Have an empty Magic Box in slot 10
Sub cHoHTokenCombineLogic
|This is the slot of the Magic Box I'll be using. Use 10 since I'm using Auto Inventory
/declare MagicBoxSlot string local pack10
|These are the items I'm going to check for, grab 2 of, and combine them in
/declare item1 string local Split Paw Quest Token
/declare item2 string local The Oceanlord Quest Token
/declare item3 string local The Lightbringer Quest Token
/declare item4 string local The Tranquil Quest Token
/declare item5 string local The Faceless Quest Token
/declare item6 string local H-One N-One Quest Token
/declare item7 string local The Prince of Darkness Quest Token
/declare i int local 1

/keypress OPEN_INV_BAGS
|Delay 1minute until that condition is met
/delay 1m ${Window[pack8].Open}

|Cycle through all 7 of the items above
/for i 1 to 7
	:nextset
	|Check if I have 2 or more of the item
	/if (${FindItem[=${item${i}}].Stack} < 2) {
	/echo I don't have enough of ${item${i}} I only have ${FindItem[=${item${i}}].Stack}. Going to next one
	/if (${Math.Calc[${i}+1]}>=8) {
		/echo Done!
		/return
	}
	/next i
	}
	|Pickup the first one
	
	/if (!${Defined[BagNum]}) /declare BagNum int local
	/varcalc BagNum ${FindItem[=${item${i}}].ItemSlot}
	/varcalc BagNum ${BagNum}-22
	/if (!${Defined[BagSlot]}) /declare BagSlot int local
	/varcalc BagSlot ${FindItem[=${item${i}}].ItemSlot2}
	/varcalc BagSlot ${BagSlot}+1
	
	
	/ctrlkey /itemnotify in pack${BagNum} ${BagSlot} leftmouseup
	/delay 1m ${Cursor.ID} == ${FindItem[=${item${i}}].ID}
	/itemnotify in ${MagicBoxSlot} 1 leftmouseup
	/delay 1m !${Cursor.ID}
	|Pickup the second one
	/ctrlkey /itemnotify in pack${BagNum} ${BagSlot} leftmouseup
	/delay 1m ${Cursor.ID} == ${FindItem[=${item${i}}].ID}
	/itemnotify in ${MagicBoxSlot} 2 leftmouseup
	/delay 1m !${Cursor.ID}
	|Hit Combine and put it away
	/combine ${MagicBoxSlot}
	/delay 1m ${Cursor.ID}
	/delay 2
	/autoinventory
	/delay 1m !${Cursor.ID}
	|If I still have more, go through the process again
	/if (${FindItem[${item${i}}].ID}) {
		/echo Restarting
		/goto :nextset
	}
	/next i

	/echo Done!

/return