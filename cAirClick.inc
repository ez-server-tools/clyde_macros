#warning


|====== Modified by Clyde for EZ Server to fit my cMacro echo system
|====== Original credit: s0rcier - http://ezserver.online/forums/index.php?topic=5621
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|======
|======
|====== This is for use in Plane of Dragons at the Emissaries to turn in crystals until you are out.
|====== Use this on an alt, not your driver.


#Event cAirClick	"cAirClick"

#Event cWasteToken 			"You cannot summon more than one diety at a time!"

|==== Sub Event_cAirClick
	Sub Event_cAirClick
		/call cAirClickLogic
	/return
|==== End Sub Event_cAirClick


Sub cAirClickLogic
    /declare AirTokens[15] string local
	/varset AirTokens[1] The Lightbringer Quest Token
    /varset AirTokens[2] The Prince of Darkness Quest Token
    /varset AirTokens[3] The Tranquil Quest Token
    /varset AirTokens[4] The Faceless Quest Token
    /varset AirTokens[5] The Oceanlord Quest Token
    /varset AirTokens[6] Split Paw Quest Token
    /varset AirTokens[7] H-One N-One Quest Token
    /varset AirTokens[8] The Rainkeeper Quest Token
    /varset AirTokens[9] The Prime Healer Quest Token
    /varset AirTokens[10] The Plaguebringer Quest Token
    /varset AirTokens[11] The Prince of Hate Quest Token
    /varset AirTokens[12] The Burning Prince Quest Token
    /varset AirTokens[13] The Mother of All Quest Token
    /varset AirTokens[14] The Warlord Quest Token
	
	
	/declare BossNames[15] string local
	/varset BossNames[1] The Lightbringer
    /varset BossNames[2] The Prince of Darkness
    /varset BossNames[3] The Tranquil
    /varset BossNames[4] The Faceless
    /varset BossNames[5] The Oceanlord
    /varset BossNames[6] Split Paw
    /varset BossNames[7] H-One N-One
    /varset BossNames[8] The Rainkeeper
    /varset BossNames[9] The Prime Healer
    /varset BossNames[10] The Plaguebringer
    /varset BossNames[11] The Prince of Hate
    /varset BossNames[12] The Burning Prince
    /varset BossNames[13] The Mother of All
    /varset BossNames[14] The Warlord
    /varset BossNames[15] The MCP
	
	/declare t int local 0
	/declare i int local
	/declare f int local 
	/declare c int local
	
	:count
    /if (${Zone.ShortName.NotEqual[airplane]}) {
	    /echo You are not in AirPlane... Get There and Rerun!
		/return
	}
	
	:MCPTURNIN
	/if (${FindItemCount[The MCP Quest Token]} >= 4) {
		/if (${SpawnCount[npc Observer of the sky radius 20]}) {
			/tar npc observer
			/stick 5
			/call TurninMCPToken		
			/docommand /${AnnounceChannel} Turnin 4x MCP Token ${FindItemCount[The MCP Quest Token]} -> ${Math.Calc[${Macro.RunTime}/60]}Min.
			/goto :MCPTURNIN
		}
	}

	/varcalc t 0
	/varcalc f 0
	/for i 1 to 14 do
	    /varcalc c ${FindItemCount[${AirTokens[${i}]}]}
		/if (${c} >0) {
  		    /varcalc t ${t} + ${c}
			/if (!${f}) /varset f ${i}
		}
	/next i
	
	/echo AirClicker.mac -> Found ${t} Tokens
	/if (!${t}) {
	    /echo AirClicker.mac -> You dont have any left... grab more!!!
		/return
	}

	:again
	/delay 5
	/doevents
    /if (${Cursor.ID}) /autoinv
	/if (${SpawnCount[npc loc -530 -210 radius 100]}>10000) {
		/goto :again
		/delay 2s
	}
	/if (!${Cast.Ready[]}) /goto :again
	/delay 5
	/echo $FindItem[${AirTokens[${f}]}].InvSlot
	
	/if (!${Defined[BagNum]}) /declare BagNum int local
	/varcalc BagNum ${FindItem[=${AirTokens[${f}]}].ItemSlot}
	/varcalc BagNum ${BagNum}-22
	/if (!${Defined[BagSlot]}) /declare BagSlot int local
	/varcalc BagSlot ${FindItem[=${AirTokens[${f}]}].ItemSlot2}
	/varcalc BagSlot ${BagSlot}+1
	
    /if (!${Spawn[npc ${BossNames[${f}]}].ID}) {
		/docommand /${AnnounceChannel} (${t}) Click ${AirTokens[${f}]} ${FindItemCount[${AirTokens[${f}]}]} -> ${Math.Calc[${Macro.RunTime}/60]}Min.
		/nomodkey /itemnotify in pack${BagNum} ${BagSlot} rightmouseup
		/if (${FindItemCount[${AirTokens[${f}]}]}==1) {
			/docommand /${AnnounceChannel} Using last token ${FindItemCount[${AirTokens[${f}]}]}. Waiting 15 seconds before next click.
			/delay 15s
		}
	}
	/goto :count
	
/return


Sub Event_cWasteToken
	/docommand /${AnnounceChannel} WASTED TOKEN ENDING ALL MACROS
	/end
/return

Sub TurninMCPToken
	/autoi
	/delay 5
	/if (!${Defined[BossUp]}) /declare BossUp int local 0
	/if (!${Defined[ItemName]}) /declare ItemName The MCP Quest Token
	/if (!${Defined[BagNum]}) /declare BagNum int local
	/varcalc BagNum ${FindItem[=${ItemName}].ItemSlot}
	/varcalc BagNum ${BagNum}-22
	/if (!${Defined[ItemBagSlot]}) /declare ItemBagSlot int local
	/varcalc ItemBagSlot ${FindItem[=${ItemName}].ItemSlot2}
	/varcalc ItemBagSlot ${ItemBagSlot}+1
	
	/delay 2s
	/varset BossUp ${CheckIfBossesUp[${ItemName}]}
	/if (!${BossUp}) {
		/itemnotify pack${BagNum} rightmouseup
		/while (!${Window[pack${BagNum}].Open}) {
			/delay 1s
		}
		
		/tar npc observer
		/stick 5
		/while (${Target.DisplayName.NotEqual[Observer of the Sky]}) {
			/delay 5
		}
		/declare Index int local 0
		/while (${Index} < 4) {
			/ctrlkey /itemnotify in pack${BagNum} ${ItemBagSlot} leftmouseup
			/while (!${Cursor.ID}) {
				/delay 5
			}
			/click left target
			/while (${Cursor.ID}) {
				/delay 5
			}
			/varcalc Index ${Index}+1
		}
		/while (!${Window[GiveWnd].Open}) {
			/delay 5
		}
		
		/notify TradeWnd TRDW_Trade_Button LeftMouseUp
		/autoi
		
		/itemnotify pack${BagNum} rightmouseup
		/while (${Window[pack${BagNum}].Open}) {
			/delay 5
		}
	}
	
/return

Sub CheckIfBossesUp(string Dummy)
	/declare BossNames[15] string local
	/varset BossNames[1] The Lightbringer
    /varset BossNames[2] The Prince of Darkness
    /varset BossNames[3] The Tranquil
    /varset BossNames[4] The Faceless
    /varset BossNames[5] The Oceanlord
    /varset BossNames[6] Split Paw
    /varset BossNames[7] H-One N-One
    /varset BossNames[8] The Rainkeeper
    /varset BossNames[9] The Prime Healer
    /varset BossNames[10] The Plaguebringer
    /varset BossNames[11] The Prince of Hate
    /varset BossNames[12] The Burning Prince
    /varset BossNames[13] The Mother of All
    /varset BossNames[14] The Warlord
    /varset BossNames[15] The MCP
	
	/declare Index int local 1
	
	/for Index 1 to 15 do
		/if (${Spawn[npc ${BossNames[${Index}]}].ID}) {
  		    /return 1
		}
	/next Index
/return 0