#warning


|====== Modified by Clyde for EZ Server to fit my cMacro echo system
|====== Original credit: Ogru - http://ezserver.online/forums/index.php?topic=6089.0
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.

#include cMacros\cCommonUtils.inc


#Event cEZProgression1	"cProg #1#"
#Event cEZProgression2	"cProgS #1# #2#"
#Event cEZProgression3	"cProgLoot #1#"


|==== Sub Event_cEZProgression
	Sub Event_cEZProgression1(Line, strArg1)
		/delay 1s
		/call cEZProgressionLogic ${strArg1}
	/return
	
	
	Sub Event_cEZProgression2(Line, strArg1, strArg2)
		/delay 1s
		/call cEZProgressionLogic ${strArg1} ${strArg2}
	/return
	
	
	Sub Event_cEZProgression3(Line, strArg1)
		/delay 1s
		
		/call cEZProgressionLogic ${strArg1}
		/delay 5s
		/call LootINILogic ${strArg1}
	/return
|==== End Sub Event_cEZProgression



Sub cEZProgressionLogic(strArg1,strArg2)
		/if (!${Defined[strArgTier]}) /declare strArgTier 				string		local
		/if (!${Defined[strArgSlot]}) /declare strArgSlot 				string		local
		
		/call SetUpOuterVariables
		/call SetupCharacter
		
		|- Has 2 Parameterss 
		/if (${strArg2.Length}>0) {
			/if (${Select[${strArg1},${TierList}]} > 0) {
				/varset strArgTier ${strArg1}
			} else /if (${Select[${strArg1},${SlotsList}]} > 0) {
				/varset strArgSlot ${strArg1}
			}
		
			/if (${Select[${strArg2},${TierList}]} > 0) {
				/varset strArgTier ${strArg2}
			} else /if (${Select[${strArg2},${SlotsList}]} > 0) {
				/varset strArgSlot ${strArg2}
			}
			
		} else { 
			/if (${Select[${strArg1},${TierList}]} > 0) {
				/varset strArgTier ${strArg1}
			} else /if (${Select[${strArg1},${SlotsList}]} > 0) {
				/varset strArgSlot ${strArg1}
			}
		}
		
		|- Has at least 1 Parameters
		/if (${strArg1.Length}==0) {
			|- no Parameterss - Show armor needed for lowest tier and all slots
			/call UpdateCompletedArmor
			/call ArmorNeeded
		} else {
			|- Has Parameters(s)
			/if (${strArg1.Equal[Completed]}) {
				|- Show highest Tier Completed for all slots
				/call UpdateCompletedArmor
				/call ArmorCompleted
			} else /if ((${strArgSlot.Length}>0) || (${strArgTier.Length}>0)) {
				|- Show Needed armor for Tier and/or Slot provided
				/call UpdateCompletedArmor "${strArgSlot}"
				/call ArmorNeeded "${strArgSlot}" "${strArgTier}"
			} else /if (${strArg1.Equal[help]}) {
				/call Help
			}
		}
		
		:TheEnd
	/Return
|------- End Sub Main

|------- Sub Help
	Sub Help
		/if (${Plugin[MQ2DanNet].Version}) {
			/dgt all \aw=============== \arEZ Progression Armor Tracker \aw===============
			/dgt all Tiers: \aoQVIC, CAZIC, T1, T2, T3, T4, T5, T6, T7, T8 \a-w(Not available - T9, T10)
			/dgt all Slots: \aoHead, Chest, Arms, Legs, Wrist, Hands, Feet
			/dgt all \ayUsage: \aw/EzProgression \atCommand  \awor  \aw/EzP \atCommand
			/dgt all -- \at(Blank)	\a-w-- Current Amror slot needed with needed pattern and component
			/dgt all -- \at*Tier*	\a-w-- Armor status for the given \aoTier
			/dgt all -- \at*Slot*	\a-w-- Armor status for the given \aoSlot
			/dgt all -- \at*Tier* *Slot* \a-w-- Armor status for the given \aoTier \a-wand \aoSlot 
			/dgt all -- \atCompleted	\a-w-- Shows the highest Tier completed for each slot
			/dgt all \ayExamples: \a-w/EzProgression \aoCAZIC\a-w, /EzProgression \aoLegs\a-w
			/dgt all \ayExamples: \a-w/EzProgression \aoT3 Chest\a-w, /EzProgression \aoCompleted
			/dgt all \aw=====================================================
		} else /if (${Plugin[MQ2EQBC].Version}) {
			/bc [+W+]=============== [+r+]EZ Progression Armor Tracker [+W+]===============
			/bc Tiers: [+o+]QVIC, CAZIC, T1, T2, T3, T4, T5, T6, T7, T8 [+x+](Not available - T9, T10)
			/bc Slots: [+o+]Head, Chest, Arms, Legs, Wrist, Hands, Feet
			/bc [+y+]Usage: [+w+]/EzProgression [+t+]Command  [+W+]or  [+w+]/EzP [+t+]Command
			/bc -- [+t+](Blank)	[+x+]-- Current Amror slot needed with needed pattern and component
			/bc -- [+t+]*Tier*	[+x+]-- Armor status for the given [+o+]Tier
			/bc -- [+t+]*Slot*	[+x+]-- Armor status for the given [+o+]Slot
			/bc -- [+t+]*Tier* *Slot* [+x+]-- Armor status for the given [+o+]Tier [+x+]and [+o+]Slot 
			/bc -- [+t+]Completed	[+x+]-- Shows the highest Tier completed for each slot
			/bc [+y+]Examples: [+x+]/EzProgression [+o+]CAZIC[+x+], /EzProgression [+o+]Legs[+x+]
			/bc [+y+]Examples: [+x+]/EzProgression [+o+]T3 Chest[+x+], /EzProgression [+o+]Completed
			/bc [+W+]=====================================================
		}
		/delay 1
	/Return
|------- End Sub Help

|------- Sub SetupCharacter
	Sub SetupCharacter
		/if (!${Defined[i]}) /declare i				int		local 	6
		/varset i 6
		/if (!${Defined[x]}) /declare x				int		local 	6
		/varset x 6
		
		/if (${Ini[${EZCharIni}].Length}<1) {
			/if (${Plugin[MQ2EQBC].Version}) /bc No EZProgression INI file found for [+y+]${Me}[+w+], creating one.
			/if (${Plugin[MQ2DanNet].Version}) /dgt all No EZProgression INI file found for \ay${Me}\aw, creating one.
			/alias /EzProgression /mac EzProgression
			/alias /EzP /mac EzProgression

			/ini ${EZCharIni} "Settings" "ShowWrist2" "FALSE"
			/for i 1 to ${Tiers.Count[|]}
				/ini ${EZCharIni} "${i}" "Tier" ${Tiers.Arg[${i},|]}
				/for x 1 to ${Slots.Count[|]}
					/ini ${EZCharIni} "${i}" "${Slots.Arg[${x},|]}_Completed" FALSE
				/next x
			/next i
		}
	/Return
|------- End Sub SetupCharacter

|------- Sub ArmorCompleted
	Sub ArmorCompleted
		
		/if (!${Defined[t]}) 			/declare t						int		local
		/if (!${Defined[s]}) 			/declare s						int		local 
		/if (!${Defined[intTier]}) 		/declare intTier 				int		local
		/if (!${Defined[strSlot]}) 		/declare strSlot				string	local
		/if (!${Defined[blnTierDone]})  /declare blnTierDone			bool	local	FALSE
		/varset blnTierDone FALSE
		
		|Slots
		/for s 1 to ${Slots.Count[|]}
			/varset strSlot ${Slots.Arg[${s},|]}
			/varset blnTierDone FALSE
			|Tiers
			/for t ${Tiers.Count[|]} downto 1
				/if (!${blnTierDone}) {
					/varset intTier ${t}
						
					/if (${Ini[${EZCharIni},"${intTier}","${strSlot}_Completed"]}==TRUE) {
						/if (${Plugin[MQ2DanNet].Version}) /dgt all \at${Tiers.Arg[${t},|]} \aw| \ao${strSlot} \aw| \agCompleted
						/if (${Plugin[MQ2EQBC].Version}) /bc [+t+]${Tiers.Arg[${t},|]} [+w+]| [+o+]${strSlot} [+w+]| [+g+]Completed
						/varset blnTierDone TRUE
					}
				}
			/next t
		/next s
		/delay 1
	/Return
|------- End Sub ArmorCompleted

|------- Sub UpdateCompletedArmor
	Sub UpdateCompletedArmor(strArgSlot)

		/if (!${Defined[t]})              /declare t					int		local
		/if (!${Defined[tp1]}) 			  /declare tp1					int 	local
		/if (!${Defined[s]}) 			  /declare s					int		local
		/if (!${Defined[SlotStart]}) 	  /declare SlotStart			int		local
		/if (!${Defined[SlotEnd]}) 		  /declare SlotEnd				int		local
		/if (!${Defined[i]}) 			  /declare i					int		local 
		
		/if (!${Defined[intTier]}) 		  /declare intTier 				int		local
		/if (!${Defined[strSlot]}) 		  /declare strSlot				string	local
		/if (!${Defined[strArmor]}) 	  /declare strArmor				string	local
		/if (!${Defined[intSlotFound]})   /declare intSlotFound			int 	local
		/if (!${Defined[blnTierDone]}) 	  /declare blnTierDone			bool	local	FALSE
		/varset blnTierDone FALSE
		
		|- 1 Parameters
		/if (${strArgSlot.Length}>0) {
			
			/if (${Select[${strArgSlot.Left[4]},${SlotsShort}]}==5) {
				/varset SlotStart 5
				/varset SlotEnd 6
			} else {
				/varset SlotStart ${Select[${strArgSlot.Left[4]},${SlotsShort}]}
				/varset SlotEnd ${Select[${strArgSlot.Left[4]},${SlotsShort}]}
			}
		} else {
			|- no Parameterss 
			/varset SlotStart 1
			/varset SlotEnd ${Slots.Count[|]}
		}
		
		/for s ${SlotStart} to ${SlotEnd}
			/varset strSlot ${Slots.Arg[${s},|]}
			/varset blnTierDone FALSE
			
			/for t ${Tiers.Count[|]} downto 1
				/if (!${blnTierDone}) {
					/varset intTier ${t}
					
					/varset strArmor ${Ini[${EZArmorIni},${intTier},${MyClass}_${strSlot}]}
					
					|- If Tier_Slot not completed
					/if (!${Ini[${EZCharIni},${t},${strSlot}_Completed]}) {
						
						/varset intSlotFound ${Math.Calc[${FindItemCount["=${Ini[${EZArmorIni},${intTier},${MyClass}_${strSlot}]}"]} + ${FindItemBankCount["=${Ini[${EZArmorIni},${intTier},${MyClass}_${strSlot}]}"]}]}
						
						/if (${strSlot.Equal[Wrist2]}) {
							|--Check if Wirst 1 is completed 
							/varcalc tp1 ${intTier}+1
			
							/if (${Ini[${EZCharIni},${tp1},Wrist1_Completed]}) {
								/if (${intSlotFound}>0) {
									/ini ${EZCharIni} "${intTier}" "${strSlot}_Completed" "TRUE"
									/if (${intTier}>1) {
									
										/for i ${intTier}-1 downto 1
											/ini ${EZCharIni} "${i}" "${strSlot}_Completed" "TRUE"
										/next i
									}
								
									/varset blnTierDone TRUE
								}
							} else {
								/if (${intSlotFound}>1) {
									/ini ${EZCharIni} "${intTier}" "${strSlot}_Completed" "TRUE"
									/if (${intTier}>1) {
									
									/for i ${intTier}-1 downto 1
										/ini ${EZCharIni} "${i}" "${strSlot}_Completed" "TRUE"
									/next i
									}
								
									/varset blnTierDone TRUE
								}
							}
							
						} else {
							/if (${intSlotFound}>0) {
								/ini ${EZCharIni} "${intTier}" "${strSlot}_Completed" "TRUE"
								
								/if (${intTier}>1) {
									
									/for i ${intTier}-1 downto 1
										/ini ${EZCharIni} "${i}" "${strSlot}_Completed" "TRUE"
									/next i
								}
								/varset blnTierDone TRUE
							}
						}
					}
				}

			/next t
		/next s
		
	/Return
|------- End Sub UpdateCompletedArmor



|------- Sub ArmorNeeded
	Sub ArmorNeeded(strArgSlot, strArgTier, SetupLootFlag)
		/echo slot: ${strArgSlot}
		/echo tier: ${strArgTier}
		
		/if (!${Defined[t]})   				  /declare t					int		local
		/if (!${Defined[TierStart]}) 		  /declare TierStart			int		local
		/if (!${Defined[TierEnd]})			  /declare TierEnd				int		local
		/if (!${Defined[tp1]})				  /declare tp1					int 	local
		/if (!${Defined[s]}) 				  /declare s					int		local 
		/if (!${Defined[SlotStart]})		  /declare SlotStart			int		local
		/if (!${Defined[SlotEnd]}) 			  /declare SlotEnd				int		local
		/if (!${Defined[intTier]}) 			  /declare intTier 				int		local
		/if (!${Defined[strSlot]}) 			  /declare strSlot				string	local
		/if (!${Defined[intPatternCount]}) 	  /declare intPatternCount 		int 	local  	0
		/varset intPatternCount 0
		/if (!${Defined[intComponentCount]})  /declare intComponentCount 	int 	local  	0
		/varset intComponentCount 0
		/if (!${Defined[strPattern]}) 		  /declare strPattern			string	local
		/if (!${Defined[strComponent]}) 	  /declare strComponent			string	local
		/if (!${Defined[strHave]}) 			  /declare strHave 				string	local 
		/if (!${Defined[strNeed]}) 			  /declare strNeed				string 	local
		/if (!${Defined[blnTierDone]})		  /declare blnTierDone			bool	local	FALSE
		/if (!${Defined[strDragonCls]}) 	  /declare strDragonCls			string	local

		/if (!${Defined[strMsgHead]})		  /declare strMsgHead 			string 	local
		/if (!${Defined[strMsgChest]})		  /declare strMsgChest 			string 	local
		/if (!${Defined[strMsgArms]}) 		  /declare strMsgArms 			string 	local
		/if (!${Defined[strMsgLegs]})		  /declare strMsgLegs 			string 	local
		/if (!${Defined[strMsgWrist1]})		  /declare strMsgWrist1 		string 	local
		/if (!${Defined[strMsgWrist2]})		  /declare strMsgWrist2	 		string	local
		/if (!${Defined[strMsgHands]}) 		  /declare strMsgHands 			string 	local
		/if (!${Defined[strMsgFeet]}) 		  /declare strMsgFeet 			string 	local
		
		/if (${strArgSlot.Length}>0) {
			
			/if (${Select[${strArgSlot.Left[4]},${SlotsShort}]}==5) {
				/varset SlotStart 5
				/varset SlotEnd 6
			} else {
				/varset SlotStart ${Select[${strArgSlot.Left[4]},${SlotsShort}]}
				/varset SlotEnd ${Select[${strArgSlot.Left[4]},${SlotsShort}]}
			}
		} else {
			/varset SlotStart 1
			/varset SlotEnd ${Slots.Count[|]}
		}
		
		/if (${strArgTier.Length}>0) {
			/if (${Select[${strArgTier},${TierList}]}>0) {	
				/varset TierStart ${Select[${strArgTier},${TierList}]}
				/varset TierEnd ${Select[${strArgTier},${TierList}]}

			} else {
				/varset TierStart 1
				/varset TierEnd ${Tiers.Count[|]}
			}
		} else {
			/varset TierStart 1
			/varset TierEnd ${Tiers.Count[|]}
		}
		
		/for s ${SlotStart} to ${SlotEnd}
			/varset strSlot ${Slots.Arg[${s},|]}
			/varset blnTierDone FALSE
			
			/for t ${TierStart} to ${TierEnd}
				/varset intTier ${t}
				
				/if (!${blnTierDone}) {
					
					/varset strPattern ${Ini[${EZArmorIni},${intTier},${MyClass}_${strSlot}_Pattern]}
					/varset strComponent ${Ini[${EZArmorIni},${intTier},${MyClass}_${strSlot}_Comp]}
					
					/varset strHave 
					/varset strNeed 
					
					/if (!${Ini[${EZCharIni},"${intTier}","${strSlot}_Completed"]}) {
						
						/varset intPatternCount ${Math.Calc[${FindItemCount[=${strPattern}]} + ${FindItemBankCount[=${strPattern}]}]}
						
						|- T2 Dragon Class Armor Slots
						/if (${intTier}==4) {
							/if ((${strSlot.Equal[Wrist1]}) || (${strSlot.Equal[Wrist2]})) {
								/varset strDragonCls Dragon Class ${strSlot.Left[5]} Slot
							} else /if (${strSlot.Equal[Hands]}) {
								/varset strDragonCls Dragon Class ${strSlot.Left[4]} Slot
							} else {
								/varset strDragonCls Dragon Class ${strSlot} Slot
							}
							
							/varset intPatternCount ${Math.Calc[${intPatternCount} + ${FindItemCount[=${strDragonCls}]} + ${FindItemBankCount[=${strDragonCls}]}]}
						}
						
						/if (!${strComponent.Equal[Null]}) {
							/varset intComponentCount ${Math.Calc[${FindItemCount[=${strComponent}]} + ${FindItemBankCount[=${strComponent}]}]}
						}
						
						/if (${strSlot.Equal[Wrist2]}) {
							|--Check if Wirst 1 is completed 
							/varcalc tp1 ${intTier}
			
							/if (${Ini[${EZCharIni},"${tp1}","Wrist1_Completed"]}) {
							
								
								|-- PATTERN ---------------------------------------
								/if (${intPatternCount}>0) {
									/call HaveMessage "${strHave}" "${intPatternCount}" "0"
									/varset strHave ${Macro.Return}
								} else {
									/call NeedMessage "${strNeed}" "${strPattern}"
									/varset strNeed ${Macro.Return}
								}
								
								|-- COMPONENT -------------------------------------
								/if (!${strComponent.Equal[Null]}) {
									/if (${intComponentCount}>0) {
										/call HaveMessage "${strHave}" "0" "${intComponentCount}"
										/varset strHave ${Macro.Return}
									} else {
										/call NeedMessage "${strNeed}" "${strComponent}"
										/varset strNeed ${Macro.Return}
									}
								}
							
					
							} else {
								|-- PATTERN ---------------------------------------
								/if (${intPatternCount}>1) {
									/call HaveMessage "${strHave}" "${intPatternCount}" "0"
									/varset strHave ${Macro.Return}
								} else {
									/call NeedMessage "${strNeed}" "${strPattern}"
									/varset strNeed ${Macro.Return}
								}
								
								|-- COMPONENT -------------------------------------
								/if (!${strComponent.Equal[Null]}) {
									/if (${intComponentCount}>1) {
										/call HaveMessage "${strHave}" "0" "${intComponentCount}"
										/varset strHave ${Macro.Return}
									} else {
										/call NeedMessage "${strNeed}" "${strComponent}"
										/varset strNeed ${Macro.Return}
									}
								}
							}
						} else {
							|-- PATTERN ---------------------------------------
							/if (${intPatternCount}>0) {
								/call HaveMessage "${strHave}" "${intPatternCount}" "0"
								/varset strHave ${Macro.Return}
							} else {
								/call NeedMessage "${strNeed}" "${strPattern}"
								/varset strNeed ${Macro.Return}
							}
							
							|-- COMPONENT -------------------------------------
							/if (!${strComponent.Equal[Null]}) {
								/if (${intComponentCount}>0) {
									/call HaveMessage "${strHave}" "0" "${intComponentCount}"
									/varset strHave ${Macro.Return}
								} else {
									/call NeedMessage "${strNeed}" "${strComponent}"
									/varset strNeed ${Macro.Return}
								}
							}
						}
						
						|- Message with slot information
						/if (${Plugin[MQ2EQBC].Version}) /varset strMsg${strSlot} [+t]${strSlot} [+w+]| [+o+]${Tiers.Arg[${t},|]} ${strNeed} ${strHave}
						/if (${Plugin[MQ2DanNet].Version}) /varset strMsg${strSlot} \at${strSlot} \aw| \ao${Tiers.Arg[${t},|]} ${strNeed} ${strHave}
						/varset blnTierDone TRUE
					}
				}
				
			/next t

		/next s
		/if (${SetupLootFlag.Equal[1]}) {
			/if (!${Defined[LootINI]}) /declare LootINI string outer cMacros\cLootIni\cLoot_${Tiers.Arg[${intTier},|]}_${MyClass}.ini
			|/if (!${Defined[LootINI]}) /declare LootINI string outer cMacros\cTurboLootIni\cTurboLoot_${Tiers.Arg[${intTier},|]}_${Me.Name}.ini
			/call SetupLootINI
			/echo ${intTier}
			/if (${intTier}==10) {
				/if (${strMsgArms.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Arms]}" Keep
				}
				/if (${strMsgChest.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Chest]}" Keep
				}
				/if (${strMsgFeet.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Feet]}" Keep
				}
				/if (${strMsgHands.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Hands]}" Keep
				}
				/if (${strMsgHead.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Head]}" Keep
				}
				/if (${strMsgLegs.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Legs]}" Keep
				}
				/if (${strMsgWrist1.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Wrist1]}" Keep
				}
				
				/ini ${LootINI} "SomeItems" "Silver Veeshan Scales" 3
				/ini ${LootINI} "SomeItems" "Red Veeshan Scales" 3
				/ini ${LootINI} "SomeItems" "Green Veeshan Scales" 3
			} else {
				/if (${strMsgArms.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Arms_Pattern]}" Keep
				}
				/if (${strMsgChest.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Chest_Pattern]}" Keep
				}
				/if (${strMsgFeet.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Feet_Pattern]}" Keep
				}
				/if (${strMsgHands.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Hands_Pattern]}" Keep
				}
				/if (${strMsgHead.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Head_Pattern]}" Keep
				}
				/if (${strMsgLegs.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Legs_Pattern]}" Keep
				}
				/if (${strMsgWrist1.Length} || 1) {
					/ini ${LootINI} "SingleItems" "${Ini[${EZArmorIni},${intTier},${MyClass}_Wrist1_Pattern]}" Keep
				}
			}
			
			
		} else /if (${Plugin[MQ2DanNet].Version}) {
			/if (${strMsgHead.Length}) /dgt all ${strMsgHead}
			/if (${strMsgChest.Length}) /dgt all ${strMsgChest}
			/if (${strMsgArms.Length}) /dgt all ${strMsgArms}
			/if (${strMsgLegs.Length}) /dgt all ${strMsgLegs}
			/if (${strMsgWrist1.Length}) /dgt all ${strMsgWrist1}
			|/if (${strMsgWrist2.Length}) /dgt all ${strMsgWrist2}
			/if (${strMsgHands.Length}) /dgt all ${strMsgHands}
			/if (${strMsgFeet.Length}) /dgt all ${strMsgFeet}
			
			
			|/if (${strMsgHead.Find[NEED]})   /mqlog /dgt all ${strMsgHead}
			|/if (${strMsgChest.Find[NEED]})  /mqlog /dgt all ${strMsgChest}
			|/if (${strMsgArms.Find[NEED]})   /mqlog /dgt all ${strMsgArms}
			|/if (${strMsgLegs.Find[NEED]})   /mqlog /dgt all ${strMsgLegs}
			|/if (${strMsgWrist1.Find[NEED]}) /mqlog /dgt all ${strMsgWrist1}
			|/if (${strMsgHands.Find[NEED]})  /mqlog /dgt all ${strMsgHands}
			|/if (${strMsgFeet.Find[NEED]})   /mqlog /dgt all ${strMsgFeet}
		} else /if (${Plugin[MQ2EQBC].Version}) {
			/if (${strMsgHead.Length}) /bc ${strMsgHead}
			/if (${strMsgChest.Length}) /bc ${strMsgChest}
			/if (${strMsgArms.Length}) /bc ${strMsgArms}
			/if (${strMsgLegs.Length}) /bc ${strMsgLegs}
			/if (${strMsgWrist1.Length}) /bc ${strMsgWrist1}
			|/if (${strMsgWrist2.Length}) /bc ${strMsgWrist2}
			/if (${strMsgHands.Length}) /bc ${strMsgHands}
			/if (${strMsgFeet.Length}) /bc ${strMsgFeet}
		}
		/delay 6
		
		:TheEnd
	/Return
|------- End Sub ArmorNeeded

|------- Sub SetUpOuterVariables
	Sub SetUpOuterVariables
	
		/if (!${Defined[EZCharIni]}) /declare EZCharIni					string 		outer 	cMacros\cEZProgressionIni\EZProgression_${Me}.ini
		/if (!${Defined[EZArmorIni]}) /declare EZArmorIni				string 		outer 	cMacros\cEZProgressionIni\EZProgression_Armor_List.ini
		/if (!${Defined[MyClass]}) /declare MyClass						string 		outer 	${Me.Class.ShortName}
		/if (!${Defined[Tiers]}) /declare Tiers 						string 		outer 	QVIC|CAZIC|T1|T2|T3|T4|T5|T6|T7|T8|T9|
		/if (!${Defined[TierList]}) /declare TierList					string 		outer 	QVIC,CAZIC,T1,T2,T3,T4,T5,T6,T7,T8,T9
	    /if (!${Defined[Slots]}) /declare Slots							string		outer	Head|Chest|Arms|Legs|Wrist1|Wrist2|Hands|Feet|
		/if (!${Defined[SlotsList]}) /declare SlotsList					string		outer	Head,Chest,Arms,Legs,Wrist,Hands,Feet
		/if (!${Defined[SlotsShort]}) /declare SlotsShort				string		outer	Head,Ches,Arms,Legs,Wris,Wris,Hand,Feet
		/if (!${Defined[ShowWrist2]}) /declare ShowWrist2				bool		outer	${Ini[${EZCharIni},Settings,ShowWrist2]}

		/delay 1
	/return
|------- End Sub SetUpOuterVariables

|------- Sub NeedMessage
	Sub NeedMessage(strNeed, strItem)
		
		/if (${strNeed.Length}>0)  {
			/varset strNeed ${strNeed}, '${strItem}'
		} else {
			/if (${Plugin[MQ2EQBC].Version}) /varset strNeed [+w+]| [+r+]NEED: '${strItem}'
			/if (${Plugin[MQ2DanNet].Version}) /varset strNeed \aw| \arNEED: '${strItem}'
		}
		
	/Return ${strNeed}
|------- End Sub NeedMessage

|------- Sub HaveMessage
	Sub HaveMessage(strHave, PatternCount, Component1Count)
		/if (${strHave.Length}>0) {
			/if (${PatternCount}>0) {
				/varset strHave ${strHave}, PatternQty:${PatternCount}
			}
		} else {
			/if (${PatternCount}>0) {
				/if (${Plugin[MQ2EQBC].Version}) /varset strHave [+w+]| [+g+]Have: PatternQty:${PatternCount}
				/if (${Plugin[MQ2DanNet].Version}) /varset strHave \aw| \agHave: PatternQty:${PatternCount}
			}
		}
		
		/if (${strHave.Length}>0) {
			/if (${Component1Count}>0) {
				/varset strHave ${strHave}, ComponentQty:${Component1Count}
			}
		} else {
			/if (${Component1Count}>0) {
				/if (${Plugin[MQ2EQBC].Version}) /varset strHave [+w+]| [+g+]Have: ComponentQty:${Component1Count}
				/if (${Plugin[MQ2DanNet].Version}) /varset strHave \aw| \agHave: ComponentQty:${Component1Count}
				
			}
		}

	/Return ${strHave}
|------- End Sub HaveMessage

|------- Sub SetupLootINI
	Sub LootINILogic(strTier)
		/call SetUpOuterVariables
		/call ArmorNeeded "" "${strTier}" "1"
	/Return
|------- End Sub SetupLootINI

|------- Sub SetupLootINI
	Sub SetupLootINI
	
		/if (!${Defined[Plate]})   /declare	 Plate			string		local 	war|pal|clr|brd|shd
		/if (!${Defined[Cloth]})   /declare	 Cloth			string		local 	enc|mag|nec|wiz
		/if (!${Defined[Chain]})   /declare  Chain			string		local 	shm|ber|rog|rng
		/if (!${Defined[Leather]}) /declare  Leather		string		local 	dru|mnk|bst
		
		/if (!${Defined[ArmorType]}) /declare  ArmorType		string		local
	
		/if (${Plate.Find[${Me.Class.ShortName}]}) {
			/varset ArmorType Plate
		} else /if (${Cloth.Find[${Me.Class.ShortName}]}) {
			/varset ArmorType Cloth
		} else /if (${Chain.Find[${Me.Class.ShortName}]}) {
			/varset ArmorType Chain
		} else /if (${Leather.Find[${Me.Class.ShortName}]}) {
			/varset ArmorType Leather
		}
	
		/if (${Ini[${LootINI}].Length}<1) {
			/if (${Plugin[MQ2EQBC].Version}) /bc No Loot INI file found for [+y+]${Me}[+w+], creating one.
			/if (${Plugin[MQ2DanNet].Version}) /dgt all No Loot INI file found for \ay${Me}\aw, creating one.			
			
			/ini ${LootINI} "LootAll"
			/ini ${LootINI} "SomeItems"
			/ini ${LootINI} "SingleItems"
			/ini ${LootINI} "Destroy"
			/ini ${LootINI} "Parked"
		}
	/Return
|------- End Sub SetupLootINI