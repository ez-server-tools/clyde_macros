#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.


#Event cOffensive	"cOffensive"


|==== Sub Event_cOffensive
	Sub Event_cOffensive
		/call cOffensiveLogic
	/return
|==== End Sub Event_cOffensive


Sub cOffensiveLogic

	/if (${Me.Class.Name.Equal[Warrior]}) {
		/call OffensiveWarrior
	}
	/if (${Me.Class.Name.Equal[Berserker]}) {
		/call OffensiveBerserker
	}
	/if (${Me.Class.Name.Equal[Shaman]}) {
		/call OffensiveShaman
	}
	/if (${Me.Class.Name.Equal[Cleric]}) {
		/call OffensiveCleric
	}
	/if (${Me.Class.Name.Equal[Bard]}) {
		/call OffensiveBard
	}
	/if (${Me.Class.Name.Equal[Paladin]}) {
		/call OffensivePaladin
	}
	/if (${Me.Class.Name.Equal[Druid]}) {
		/call OffensiveDruid
	}
	/if (${Me.Class.Name.Equal[Rogue]}) {
		/call OffensiveRogue
	}
	/if (${Me.Class.Name.Equal[Ranger]}) {
		/call OffensiveRanger
	}
	/if (${Me.Class.Name.Equal[Necromancer]}) {
		/call OffensiveNecromancer
	}
/return

|-------------

Sub OffensiveWarrior
	
	/if (${Me.CombatAbilityReady[Savage Onslaught Discipline]}) {
		
		/disc Savage Onslaught Discipline
	}
	/if (${Me.CombatAbilityReady[Precision Discipline]}) {
		/disc Precision Discipline
	}
	
/return


Sub OffensiveBerserker

	/if (${Me.CombatAbilityReady[Vengeful Flurry Discipline]}) {
		/if (${Me.ActiveDisc.Name.Find[Bashful Crustaceans]}) {
			/stopdisc
			/delay 3
		}
	
		/disc Vengeful Flurry Discipline
	}
	
	/if (${Me.CombatAbilityReady[Cleaving Rage Discipline]}) {
		/if (${Me.ActiveDisc.Name.Find[Bashful Crustaceans]}) {
			/stopdisc
			/delay 3
		}
	
		/disc Cleaving Rage Discipline
	}
	
	| Parsed under Pincer
	|/if (${Me.CombatAbilityReady[unpredictable rage Discipline]}) {
	|	/disc unpredictable rage Discipline
	|}
		
/return


Sub OffensiveShaman

	/if (${Cast.Ready[Rabid Bear]}) /casting "Rabid Bear"
	|/if (${Cast.Ready[Spirit Call]}) /casting "Spirit Call"
	|/if (${Cast.Ready[Virulent Paralysis]}) /casting "Virulent Paralysis"
	
/return


Sub OffensiveCleric

	|/if (${Cast.Ready[Divine Avatar]}) /casting "Divine Avatar"
	
/return


Sub OffensiveBard

	/if (${Me.CombatAbilityReady[thousand blades]}) {
		/disc thousand blades
	}
	
	/if (${Cast.Ready[Cacophony]}) /casting "Cacophony"
	/if (${Cast.Ready[Dance of Blades]}) /casting "Dance of Blades"
	/if (${Cast.Ready[Funeral Dirge]}) /casting "Funeral Dirge"
	
/return


Sub OffensivePaladin

	/if (${Me.CombatAbilityReady[Holyforge Discipline]}) {
		/disc Holyforge Discipline
	}
	|/if (${Cast.Ready[Divine Stun]}) /casting "Divine Stun"
	
/return


Sub OffensiveDruid


	
	
/return


Sub OffensiveRogue

	/if (${Me.CombatAbilityReady[Frenzied Stabbing Discipline]}) {
		/if (${FindItem[Nightshade, Blade of Entropy 2.5].ID}) {
			/if (${Me.ActiveDisc.Name.Find[Deadly Precision Discipline]}) {
				/stopdisc
				/delay 3
			}			
		}
	
		/disc Frenzied Stabbing Discipline
	}

	/if (${Me.CombatAbilityReady[Twisted Chance Discipline]}) {
		/if (${FindItem[Nightshade, Blade of Entropy 2.5].ID}) {
			/if (${Me.ActiveDisc.Name.Find[Deadly Precision Discipline]}) {
				/stopdisc
				/delay 3
			}			
		}
	
		/disc Twisted Chance Discipline
	}
	
	/if (${FindItem[Nightshade, Blade of Entropy 2.5].ID}) {
		/if (${Me.CombatAbilityReady[Deadly Precision Discipline]}) {
			/disc Deadly Precision Discipline
		}
	}
	
	| Parsed on QRG dummy, and it performed the worst
	| I will note, these parses aren't great since the dummy isn'the
	| going to have the same defenses as regular tier mobs
	|/if (${Me.CombatAbilityReady[Weapon Affinity Discipline]}) {
	|	/disc Weapon Affinity Discipline
	|}

/return


Sub OffensiveRanger

	/if (${Me.CombatAbilityReady[Warder's Wrath]}) {
		/disc Warder's Wrath
	}
	
	/if (${Cast.Ready[Auspice of the Hunter]}) /casting "Auspice of the Hunter"
	
	
	/if (${Cast.Ready[Outrider's Attack]}) /casting "Outrider's Attack"
	
	
/return


Sub OffensiveNecromancer


	
	
/return

|----------