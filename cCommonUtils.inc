#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|======
|====== cCommonUtils.inc is a file to house common logic I'm using in multiple files
|====== as to always have the most up to date logic in every file.
|======

|====================================================================================

|------ cNumOpenSlots - Param: name (Dummy string... I'm not sure why I can't invoke it without a param)
|------ 
|------ Will return the amount of open slots in your inventory
|------ 
|------ Below is a sample invocation with the placeholder string to let the compiler know it's a function
|------ 
|------ /if (${cNumOpenSlots[PlaceHolder]}) /cast 1
|------ Note: cNumOpenSlots is multi-purpose. In the above example since we aren't
|------ evaluating what the return number is, were just seeing if it's true.
|------ MQ2 evaluates 0 as false and any other number as true, so in this case.
|------ It will cast 1 if there are no open inventory slots.
|------ This makes this function multi-purpose to see how many inventory slots are open
|------ or if your inventory is full or not.
|------ I've used ( ${Me.FreeInventory} ) before, but it seems buggy and not reliable so I made this
Sub cNumOpenSlots(string Name)
	/declare OpenSlotCount int local 0
	/declare PackNumber int local
	/declare SlotNumber int local
	
	/for PackNumber 10 downto 1 {
		/for SlotNumber 10 downto 1 {
			/if (!${InvSlot[pack${PackNumber}].Item.Item[${SlotNumber}].ID}) {
				/varcalc OpenSlotCount ${OpenSlotCount}+1			
			}
			/next SlotNumber
		}
		/next PackNumber
	}
/return ${OpenSlotCount}


|------ cCastProperSpellRank - Param: Command
|------ 
|------ Command is a Comma seperated string with 2 components
|------ Value 1: SpellName - The name of the spell
|------ Value 2: MGB - Add something here If you want MGB to be cast before the spell
|------ Value 3: Post cast delay (true,false) set this to true for buff routines
|------ 
|------ Sample command
|------ 
|------ Ex1: "Howl of the Huntmaster" - This will cast Howl without a post cast delay. Not good for buff sequences, but good for a spam macro
|------ Ex2: "Howl of the Huntmaster,MGB" - This will cast Howl with MGB
|------ Ex2: "Howl of the Huntmaster,MGB,true" - This will cast Howl with MGB with a post cast delay for buff sequences
|------ Ex2: "Howl of the Huntmaster,,true" - This will cast Howl without MGB with a post cast delay for buff sequences
|------ 									  Notice that the extra comma is required if we want a post cast delay but no MGB
|------ Note1: Add no spaces after the commas
|------ Note2: Only Spell name is required
|------ Note3: The 2nd arg for MGB needs to be "MGB"
sub cCastProperSpellRank(string Command)

	/if (!${Defined[SpellName]})	 			/declare SpellName string local
	/if (!${Defined[MGB]})						/declare MGB string local
	/if (!${Defined[PostCastDelay]})			/declare PostCastDelay bool local
	/if (!${Defined[SuffixNumber]})				/declare SuffixNumber int local
	/if (!${Defined[HasCasted]})		        /declare HasCasted bool local
	/if (!${Defined[CombinedSpellName]})		/declare CombinedSpellName string local
	/if (!${Defined[RecastTime]})				/declare RecastTime float local
	
	/if (!${Defined[Suffix]})				/declare Suffix[6] string local
	/varset Suffix[6] X
	/varset Suffix[5] IX
	/varset Suffix[4] V
	/varset Suffix[3] IV
	/varset Suffix[2] III
	/varset Suffix[1] II
	
	/varset SpellName ${Command.Token[1,,]}
	/varset MGB ${Command.Token[2,,]}
	/varset PostCastDelay ${Command.Token[3,,]}
	/varset SuffixNumber 7
	/varset HasCasted false
	
	/delay 1m ${Cast.Ready}	
	
	/if (${MGB.Equal[MGB]} && ${Cast.Ready[Mass Group Buff]}) {
		/echo MGBMGBMGBMGB
		/casting "Mass Group Buff" alt -maxtries|1
		/delay 1m ${Cast.Status.Equal[I]}
	}
	
	/for SuffixNumber 6 downto 1 {
		/varset CombinedSpellName "${SpellName} ${Suffix[${SuffixNumber}]}"
		/if (${Cast.Ready[${CombinedSpellName}]}) {
			/varset RecastTime ${Math.Calc[${Spell[${CombinedSpellName}].CastTime}/100]}
			/if (${RecastTime} > 100) {
				/varset RecastTime 70
			} else /if (${RecastTime} < 40) {
				/varset RecastTime 40
			}
			/casting "${SpellName} ${Suffix[${SuffixNumber}]}"
			/if (${PostCastDelay}) {
				/delay ${RecastTime}
			}
			/varset HasCasted true
		}
		/next SuffixNumber
	}
	
	/if (!${HasCasted}) {
		/if (${Cast.Ready[${SpellName}]}) {
			/varset RecastTime ${Math.Calc[${Spell[${SpellName}].CastTime}/100]}
			/if (${RecastTime} > 100) {
				/varset RecastTime 70
			} else /if (${RecastTime} < 40) {
				/varset RecastTime 40
			}
			/casting "${SpellName}" -maxtries|1
			/if (${PostCastDelay}) {
				/delay ${RecastTime}
			}
		}
	}
/return


|====================================================================================
|------ Deprecated, functions, but keeping for reference or future use


Sub cCountItemOnSelf(string ItemName)
	/declare ItemOnSelfCount int local 0
	/declare PackNumber int local
	/declare SlotNumber int local
	
	/for PackNumber 10 downto 1 {
		/for SlotNumber 10 downto 1 {
			/if (${InvSlot[pack${PackNumber}].Item.Item[${SlotNumber}].Name.Equal[${ItemName}]}) {
				/varcalc ItemOnSelfCount ${ItemOnSelfCount}+1			
			}
			/next SlotNumber
		}
		/next PackNumber
	}
/return ${ItemOnSelfCount}


|------ cCastProperSpellRank - Param: Command
|------ Was gonna be an alternate way to move characters to a target for assist, but had some issues
sub cNavigateToTarget
	/declare RetryNum int local 0


	/while (${Target.Distance} > 15) {
		/if (${RetryNum} > 5) {
			/return
		}
	
		/if (!${Navigation.PathExists[target]}) {
			/stick 10 loose
			/delay 1s
			/varcalc RetryNum ${RetryNum}+1
		} else {
			/nav target
			/while (${Navigation.Active}) {
				/if (!${Navigation.PathExists[target]}) {
					/delay 5
					/keypress esc
					/delay 5
					/stick 10
					/return
				}
				/delay 1s
			}		
		}	
	}

	

/return