#warning


|====== Modified by Clyde for EZ Server to fit my cMacro echo system
|====== Original credit: Dimurw - ezserver.online/forums/index.php?topic=5600
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|======
|======
|====== Clyde's Additions / Changes
|======	Added INI control instead of seperate macros per Toon/Zone/ETC, INI can do the same thing, but cleaner IMO
|====== Instead of a blanket loot some value, can set how many to loot in INI
|====== Added a destroy functionality


| Use this to create and load filter sets to and from an ini file.
| /echo savefilters NameToSaveAs to save a filter set named NameToSaveAs to Ini
| /echo loadfilters NameToLoad to load filter set named NameToLoad from Ini
| There are 2 pre-configured sets that are hard coded into the macro from when it was first created.
| These can be accessed with /echo setupfilters main or /echo setupfilters alt

	|#Event Help "setfilters help"
	|#Event OptionsWindowFilters "setupfilters #1#"
	|#Event SetNewFilterSet "savefilters #1#"
	|#Event SaveBlankSet "savefilters"
	|#Event ReadFromIni "loadfilters #1#"
	|#Event ReadBlankSet "loadfilters"

	#bind LoadParse_Fil /LParse
	#bind LoadBox_Fil /LBox
	#bind CreateIni_Fil /FilterSetup
	
|===============================================================================================
| SUB: Event_Help
|===============================================================================================
	Sub Event_Help
		/bc Macro will build a default filter set if no Ini file is found.
		/bc main and alt are 2 pre-configured sets that you can use without modifying the Ini
		/bc /echo setupfilters main or /echo setupfilters alt to use these
		/bc /echo savefilters NameToSaveAs to save the current filter settings to Ini
		/bc /echo loadfilters NameToLoad to load a saved filter set from the Ini
		/bc If you can't figure out that NameToSaveAs and NameToLoad can be any name you want, please delete this macro and never reference it again


|===============================================================================================
| SUB: LoadParse_Fil
|===============================================================================================
	Sub Bind_LoadParse_Fil
		/call ReadFromIni parse	
	/return


|===============================================================================================
| SUB: LoadBox_Fil
|===============================================================================================
	Sub Bind_LoadBox_Fil
		/call ReadFromIni box
	/return


|===============================================================================================
| SUB: CreateIni
|===============================================================================================
	Sub Bind_CreateIni_Fil
		/if (!${Defined[thisFilterSet]}) /declare thisFilterSet 	string 	outer
		/varset thisFilterSet ${Me}
		
		/call FilterSetLoop		
	/return
		
|===============================================================================================
| SUB: Event_OptionsWindowFilters
|===============================================================================================
	Sub Event_OptionsWindowFilters(Line, Filterset)
	
		|Set a variable named filterSet to hold the input value Filterset
		/declare filterSet string local 
		/declare Proceed int local 0
		/declare index int local
		
		|Set up various Filterset values for preset filtering
		/if (${Filterset.Equal[main]}) {
			/noparse /varset filterSet ${index} == 1 || ${index} == 2 || ${index} == 3 || ${index} == 10 || ${index} == 11 || ${index} == 20 || ${index} == 23 || ${index} == 26 ||${index} == 27 || ${index} == 28 || ${index} == 33
			/varset Proceed 1
		} else /if (${Filterset.Equal[alt]}) {
			/noparse /varset filterSet ${index} == 1 || ${index} == 2 || ${index} == 3 || ${index} == 10 || ${index} == 11 || ${index} == 23 || ${index} == 26 || ${index} == 27 || ${index} == 28
			/varset Proceed 1
		} else {
			/echo You need to use one of the pre-defined filters to apply these changes
			/echo By default, the only pre-defined filter sets are either main or alt
			/return
		}
		
		|Loop through each index in the list of filter options
		/for index 1 to ${Window[OptionsWindow].Child[OFP_FilterList].Items}
			
			|Select the index at the value of the iterator named index
			/notify OptionsChatPage OFP_FilterList listselect ${Window[OptionsWindow].Child[OFP_FilterList].List[=${Window[OptionsWindow].Child[OFP_FilterList].List[${index}]}]}
			
			|Assign show to the indexes I want the setting to be Show for, if value of index doesn't match then set it to Hide
			|If you don't want to be spammed with the settings being applied, comment out the /bc commands, it was used for debugging
			/if (${Proceed} == 1) {
				/if (${filterSet}) {
					/notify OptionsChatPage OFP_FilterComboBox listselect ${Window[OptionsWindow].Child[OFP_FilterComboBox].List[=Show]}
					
					/docommand ${BroadcastEcho}Setting ${Window[OptionsWindow].Child[OFP_FilterList].List[${index}]} to Show
				} else {
					/notify OptionsChatPage OFP_FilterComboBox listselect ${Window[OptionsWindow].Child[OFP_FilterComboBox].List[=Hide]}
					
					/docommand ${BroadcastEcho}Setting ${Window[OptionsWindow].Child[OFP_FilterList].List[${index}]} to Hide
				}
				|Add a small delay of .3 seconds so people can see the changes being applied but not be spammed too fast to read
				|Feel free to increase or decrease this delay to a value that works for you
				/delay 3
			}
			
		/next index
		
		/echo Filter set ${Filterset} has been applied, exiting this macro
		
	/end
	
|=================================================================================================
| SUB: Event_SetNewFilterSet
|=================================================================================================
	Sub Event_SetNewFilterSet(Line, SetName)
		
		/varset thisFilterSet ${SetName}
		
		/call FilterSetLoop
	/return
	
|==================================================================================================
| SUB: SaveBlankSet
|==================================================================================================
	Sub Event_SaveBlankSet
		/echo This logic fails to execute unless a name parameter is entered after /echo savefilters
	/return
	
|==================================================================================================
| SUB: FilterSetLoop
|==================================================================================================
	Sub FilterSetLoop
	
		/declare i												int 		local
		
		/for i 1 to ${Window[OptionsWindow].Child[OFP_FilterList].Items}
			/notify OptionsChatPage OFP_FilterList listselect ${Window[OptionsWindow].Child[OFP_FilterList].List[=${Window[OptionsWindow].Child[OFP_FilterList].List[${i}]}]}
			/echo Setting ${Window[OptionsWindow].Child[OFP_FilterList].List[${i}]} to: ${Window[OptionsChatPage].Child[OFP_FilterList].List[${i},2]} 

			/ini "cMacros\cFilter.ini" "${thisFilterSet}" "${i}. ${Window[OptionsWindow].Child[OFP_FilterList].List[${i}]}" "${Window[OptionsChatPage].Child[OFP_FilterList].List[${i},2]}"
			
			/delay 2
			
		/next i
		
		/docommand ${BroadcastEcho}[ ${thisFilterSet} ] filter set has been created and/or updated in the cFilter.ini file
		
	/return
	
|==================================================================================================
| SUB: Event_ReadFromIni
|==================================================================================================
	Sub ReadFromIni(FilterSetName)
		/echo In ReadFromIni
		/if (!${Defined[filterDescription]}) /declare filterDescriptions 	string 		outer		
		/declare i 						int 		local
		/declare thisDescription 		string 		local
		/declare descriptionSubstring	string		local
		/declare filterSetting			string		local
		/declare loadFilterSet			string		local	${FilterSetName}
		
		/varset filterDescriptions ${Ini[cMacros\cFilter.ini,"${loadFilterSet}"]}
		/echo Reading Filter Set ${loadFilterSet}
		
		/for i 1 to ${Window[OptionsWindow].Child[OFP_FilterList].Items}
		
			|Filter select by name logic, read it from filterDescription and select it
			/varset thisDescription ${filterDescriptions.Arg[${i},|]}
			/varset descriptionSubstring ${thisDescription.Mid[4,${Math.Calc[${thisDescription.Length}-3]}]}
			/notify OptionsChatPage OFP_FilterList listselect ${Window[OptionsChatPage].Child[OFP_FilterList].List[=${descriptionSubstring}]}
			
			|With the filter highlighted, read the value from ini and select it
			/varset filterSetting ${Ini[cMacros\cFilter.ini,${loadFilterSet},${thisDescription}]}
			/notify OptionsChatPage OFP_FilterComboBox listselect ${Window[OptionsChatPage].Child[OFP_FilterComboBox].List[=${filterSetting}]}
			
			/delay 1
			
		/next i
		
		/docommand ${BroadcastEcho}[ ${loadFilterSet} ] filter settings have been applied.
		
	/return