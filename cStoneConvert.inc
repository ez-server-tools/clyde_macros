#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.

#Event cStoneConvert		"cStoneConvert"
#Event cStoneConvert1		"cStoneConvert #1#"
#Event cStoneConvert2		"cStoneConvert #1# #2#"



|==== Sub Event_cStoneConvert
	Sub Event_cStoneConvert
		/delay 1s
		/call cStoneConvertLogic
	/return
	
	Sub Event_cStoneConvert1(Line, MaxStone)
		/delay 1s
		/call cStoneConvertLogic ${MaxStone}
	/return
	
	Sub Event_cStoneConvert2(Line, MaxStone, MinStone)
		/delay 1s
		/call cStoneConvertLogic ${MaxStone} ${MinStone}
	/return
|==== End Sub Event_cStoneConvert


Sub cStoneConvertLogic(MaxStone, MinStone)

	| ****USAGE****
	| Set the below variables for MaxStoneToConvert and PerformSmallConverts
	| Setting MaxStoneToConvert to 7 will convert 6->7 and then stop
	| Setting PerformSmallConverts to true will allow converting 2 stones at a time if there are not 8
	
	| MaxStoneToConvert Can be 2-20
	/declare MaxStoneToConvert ${Ini[${cMacIni},StoneConvert,DefaultMaxStone]}
	| MinStoneToConvert Can be 2-20
	/declare MinStoneToConvert ${Ini[${cMacIni},StoneConvert,DefaultMinStone]}
	
	| PerformSmallConverts can be "true", or "false"
	/declare PerformSmallConverts ${Ini[${cMacIni},StoneConvert,PerformSmallConverts]}
	| ONLYPerformSmallConverts set this if you only want to convert 2 at a time
	/declare ONLYPerformSmallConverts ${Ini[${cMacIni},StoneConvert,ONLYPerformSmallConverts]}
	

	
	/if (${MaxStone.Length}) {
		/varset MaxStoneToConvert ${MaxStone}
	}
	/if (${MinStone.Length}) {
		/varset MinStoneToConvert ${MinStone}
	}
	
	
	| ****WARNING****
	| Do not modify anything below this or you risk breaking functionality.


	/if (${InvSlot[pack10].Item.Name.NotEqual[A Magic Box]}) /multiline ; /echo Put a empty Magic Box in Last Slot (No.10) ; /return
	/if (${InvSlot[pack10].Item.Items}) /multiline ; /echo The Magic Box has item(s) present - unable to combine ; /return
	/if (!${PerformSmallConverts} && ${ONLYPerformSmallConverts}) /multiline ; /echo PerformSmallConverts can't be false when ONLYPerformSmallConverts is true ; /return
	/if (!${Window[pack10].Open}) /itemnotify pack10 rightmouseup


	/declare StoneArray[20] string
	
	|Stone of Heroic Resistance I
	/varset StoneArray[1] 130501
	|Stone of Heroic Resistance II
	/varset StoneArray[2] 130502
	|Stone of Heroic Resistance III
	/varset StoneArray[3] 130503
	|Stone of Heroic Resistance IV
	/varset StoneArray[4] 130504
	|Stone of Heroic Resistance V
	/varset StoneArray[5] 130505
	|Stone of Heroic Resistance VI
	/varset StoneArray[6] 130506
	|Stone of Heroic Resistance VII
	/varset StoneArray[7] 130507
	|Stone of Heroic Resistance VIII
	/varset StoneArray[8] 130508
	|Stone of Heroic Resistance IX
	/varset StoneArray[9] 130509
	|Stone of Heroic Resistance X
	/varset StoneArray[10] 130510
	|Stone of Heroic Resistance XI
	/varset StoneArray[11] 130511
	|Stone of Heroic Resistance XII
	/varset StoneArray[12] 130512
	|Stone of Heroic Resistance XIII
	/varset StoneArray[13] 130513
	|Stone of Heroic Resistance XIV
	/varset StoneArray[14] 130514
	|Stone of Heroic Resistance XV
	/varset StoneArray[15] 130515
	|Stone of Heroic Resistance XVI
	/varset StoneArray[16] 130516
	|Stone of Heroic Resistance XVII
	/varset StoneArray[17] 130517
	|Stone of Heroic Resistance XVIII
	/varset StoneArray[18] 130518
	|Stone of Heroic Resistance XIX
	/varset StoneArray[19] 130519
	|Stone of Heroic Resistance XX
	/varset StoneArray[20] 130520

	/if (!${Defined[HeroicStoneName]}) /declare HeroicStoneName outer
	/declare PackNumber int local
	/declare SlotNumber int local
	/declare StoneNum int local ${MinStoneToConvert}
	/declare CombineNumber int local
	/declare CombinePackSlotNumber int local 1
	/declare AppName string cStoneConvert
	/declare CurrentStoneCount int local 0
	/declare HeroicStoneID string local
	/declare InvFullFlag int local 0
	
	
 :StoneNumIterater
	/autoinv
	/varset HeroicStoneID ${StoneArray[${StoneNum}]}
	/if (${StoneNum} >= ${MaxStoneToConvert}) {
		
		/if (${InvFullFlag}) {
			/varset InvFullFlag 0
			/varset StoneNum ${MinStoneToConvert}
			/goto :StoneNumIterater
		}
	
		/if (${Window[pack10].Open}) /itemnotify pack10 rightmouseup
		/autoinv
		/delay 2s !${Cursor.ID}		
		/echo Done Converting
		/return
	}
	
	 :CombineLoop
		/autoinv
		/varset CurrentStoneCount ${CountAugs[${HeroicStoneID}]}
		/if (${CurrentStoneCount} <= 1 || (${CurrentStoneCount} <= 8 && !${PerformSmallConverts})) {
			/while (${Cursor.ID}) {
				/delay 1
				/autoi				
				/delay 1
			}
			/varcalc StoneNum ${StoneNum}+1
			/goto :StoneNumIterater
		}
		
		|| Only enter here if we have enough stones
		/if (${CurrentStoneCount} >= 2) {
		
			/varset CombineNumber 3
		
			/if (${CurrentStoneCount} >= 8) {
				/if (!${ONLYPerformSmallConverts}) {
					/if ((${StoneNum}+3) <= ${MaxStoneToConvert}) {
						/varset CombineNumber 9
					}
				}
			}
			
			/varset PackNumber 1
			/varset SlotNumber 1
			
			/for PackNumber 1 to 9 {
				/for SlotNumber 1 to 10 {
					/if (${InvSlot[pack${PackNumber}].Item.Item[${SlotNumber}].ID}==${HeroicStoneID}) {
						/delay 1
						/nomodkey /autoinv
						/delay 1
						/ctrl /itemnotify in pack${PackNumber} ${SlotNumber} leftmouseup
						/delay 1
						/while (!${Cursor.ID}) {
							/delay 1s ${Cursor.ID}
						}
						/delay 1
						/nomodkey /itemnotify in pack10 ${CombinePackSlotNumber} leftmouseup
						/delay 1
						/while (${Cursor.ID}) {
							/delay 1s !${Cursor.ID}
						}
						/varcalc CombinePackSlotNumber ${CombinePackSlotNumber}+1
						/if (${CombinePackSlotNumber}==${CombineNumber}) {
							/delay 1
							/nomodkey /combine pack10
							/delay 1
							/while (!${Cursor.ID}) {
								/delay 1s ${Cursor.ID}
							}
							/delay 1
							/nomodkey /autoinv
							/delay 1
							/while (${Cursor.ID}) {
								/delay 1s !${Cursor.ID}
							}
							/nomodkey /autoinv
							/delay 1
							/delay 1s !${Cursor.ID}
							/nomodkey /autoinv							
							/varset CombinePackSlotNumber 1
							/delay 1
							/if (${Me.FreeInventory}==10) {
								/varset InvFullFlag 1
								/varcalc StoneNum ${StoneNum}+1
								/goto :StoneNumIterater
							}
							
							/goto :CombineLoop
						}
					}
					/next SlotNumber
				}
				/next PackNumber
			}
			/goto :CombineLoop
		}
		/varcalc StoneNum ${StoneNum}+1
		/nomodkey /autoinv
		/goto :StoneNumIterater
	
	
/return



Sub CountAugs(string StoneID)
	/declare AugCount int local 0
	/declare PackNumber int local
	/declare SlotNumber int local
	
	
	/autoinv
	/for PackNumber 1 to 9 {
		/for SlotNumber 1 to 10 {
			/if (${InvSlot[pack${PackNumber}].Item.Item[${SlotNumber}].ID}==${StoneID}) {
				/varcalc AugCount ${AugCount}+${InvSlot[pack${PackNumber}].Item.Item[${SlotNumber}].Stack}
			}
			/next SlotNumber
		}
		/next PackNumber
	}
/return ${AugCount}