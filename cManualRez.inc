#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.


#Event cManualRez		"cManRez"



|==== Sub Event_cManualRez
	Sub Event_cManualRez
		/call cManualRezLogic
	/return
|==== End Sub Event_cManualRez

Sub cManualRezLogic
	| ADD YOUR CLERICS TO THE ARRAY, on the declare ClericArray line, make sure you make the array big enough for your amount of clerics
	/declare RezzerString string local ${Ini[${cMacIni},Clerics]}
	/declare NumRezzers int local ${RezzerString.Count[|]}
	/declare RezzerName string local
	/declare CorpseID int local

	/if (!${Defined[RaidDeathIndex]}) /declare RaidDeathIndex int local
	/if (!${Defined[RezzerIndex]}) /declare RezzerIndex int local
	
	/varcalc NumRezzers ${NumRezzers}-1
	/varset RezzerString ${RezzerString.Replace[|,,]}
	
	
	/for RaidDeathIndex 1 to ${Raid.Members} {
		/echo idx: ${RaidDeathIndex} - ${Raid.Members}
		/if (${SpawnCount[corpse ${Raid.Member[${RaidDeathIndex}].Name}]}) {
			/echo bob: ${Raid.Member[${RaidDeathIndex}]}
			/if (!${Raid.Member[${RaidDeathIndex}].Spawn.ID}) {
				/while (!${SpawnCount[pc ${RezzerString.Arg[${RezzerIndex},,]}]}) {
					/echo Rezzer not here: ${RezzerString.Arg[${RezzerIndex},,]}
					/varset RezzerIndex 1
				}
				/varset RezzerName ${RezzerString.Arg[${RezzerIndex},,]}
				/varset CorpseID ${Spawn[corpse radius 100 ${Raid.Member[${RaidDeathIndex}].Name}].ID}
				/call RezMember ${CorpseID} ${RezzerName}
				/varcalc RezzerIndex ${RezzerIndex}+1
				/if (${RezzerIndex}==${NumRezzers}) {
					/varset RezzerIndex 1
				}
			}
		}
		/next RaidDeathIndex
	}
	
/return


Sub RezMember(int CorpseID, string Rezzer)
	/dgtell all ${Rezzer} Rezzing Corpse: ${CorpseID}	
	/dquery ${Rezzer} -q "Spawn[corpse radius 100 id ${CorpseID}].ID"
	
	/if (${DanNet.Q}) {
		/dgtell all Attempting to rez ${Target.Name}
		/dex ${Rezzer} /tar id ${CorpseID}
		/delay 5
		
		/dquery ${Rezzer} -q "Cast.Ready[Blessing of Resurrection]"
		/if (${DanNet.Q}) {
			/delay 5
			/dex ${Rezzer} /casting "Blessing of Resurrection"
			/delay 5
			/return
		}
		/dquery ${Rezzer} -q "Cast.Ready[Water Sprinkler of Nem Ankh]"
		/if (${DanNet.Q}) {
			/delay 5
			/dex ${Rezzer} /casting "Water Sprinkler of Nem Ankh" item
			/delay 5
			/return
		}
		/dquery ${Rezzer} -q "Cast.Ready[Gift of Resurrection]"
		/if (${DanNet.Q}) {
			/delay 5
			/dex ${Rezzer} /casting "Gift of Resurrection"
			/delay 5
			/return
		}
		/dquery ${Rezzer} -q "Cast.Ready[Resurrection]"
		/if (${DanNEt.Q}) {
			/delay 5
			/dex ${Rezzer} /casting "Resurrection"
			/delay 5
			/return
		}
	}
/return