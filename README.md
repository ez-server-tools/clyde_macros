#DOWNLOAD WARNING
**Do not use the download button on an individual file, it will download with incorrect line endings that will break everything**
**You can download the whole repo as a zip at the project main page with the readme at the top. Press the download button and select download source as zip**



# clyde_macros

This is my collection of macros LIVE. I update the repo as I make changes if I remember.
Note: I use a Redguides compile of MQ2, and DanNet instead of EQBCS ( so /dgae instead of /bcaa )

I would like to have this as a system that new players can drag and drop to their EQ folder
and be ready to go for 1 or 6+ characters and be able to control them easily.

For now commands and filepaths will not be case sensitive "cMain cAssist" and "cmain cassist" will do the same thing.  
**Important: File structure is important, Keep the macros where they are, the INIs where they are, etc.**

###There is a TON of functionality that may not be listed here, take a look through the folder and if somethning sounds cool open it up to learn how to use it.###


## Getting started (Quick & Easy)

1. Download the entire repo. If you're getting it from Gitlab and download as ZIP it will come in a folder called "clyde_macros-main"
2. Rename the folder to "cMacros" and drop it into your MQ2 macros folder (should look something like this
    1. `"Macros\cMacros"`********
3. Create an in-game social macro which does this [a] if you use EQBC, or [b] if you use DanNet. This will be a macro always running and waiting for commands on all characters.
    1. `/bcaa /mac cMacros\cMac`
    2. `/dgae /mac cMacros\cMac`
4. Every time you log in, or change a setting in an INI (cLoot, cInstance, etc) press this ingame social macro to reload the system
5. Create in game social macros to use the system. All commands are ran with /echo and the commands from the feature list below... I'll give examples on stuff you really want.
    1. Assist Social: `/echo cMain cAssist`
    2. Follow Social: `/echo cMain cFollow`
    3. Combat Spam Social: `/echo cSpam`
    4. Stop Following Me Social: `/echo cMain cStop`
    5. Loot Social: `/echo cLoot ${Me.Name}`
        1. using `${Me.Name}` instead of your characters name allows you to write this social once and copy it to each character without having to maintin it manually.
		2. If you want to use the qvic templates, move them into the cLootIni folder, and then invoke them like this
			1. /echo cLoot ${Zone.ShortName}_${Me.Class.ShortName}
				1. This is a reusable social you can make on all characters you are progressing with, if you want you could `/noparse /bca` or `/noparse /dge` it to send the command to all other chars.
6. Set **TurboLoot** inis up for your characters.
    1. In the template folder `cMacros\cTurboLootIni\Templates` There is an empty template `cTurboLoot_Empty.ini`, take this and move it up one directory to `cMacros\cTurboLootIni` 
        1. Change "Empty" to your characters name. EX: for my Warrior Qekiibre `cTurboLoot_Qekiibre.ini`
        2. Add items you want to Keep to the ini. You can look at Qekiibre's INI for examples.
            1. In the `[LootAll]` section you can put: `Stone of Heroic Resistance VII=Keep`
                - This will loot Every Stone 7 you come across
            2. In the `[SomeItems]` section you can put: `Toothless' Wristguards=2`
                - This will loot exactly 2 of these items
            3. **DESTROY** This is contained in cMac.ini -- In the `[DestroyLoot]` section you can put: `Bard Ink=Destroy`
                - This will destroy all Bard Ink's you come across
                - Destroying things will make your loot process longer, so you may not want to destroy everything
                - **WARNING** I take no responsibility if anything gets destroyed you didn't intend. I use this functionality regularly and haven't had an issue, but you may.
            4. You may want to add a section similar to `[NotUsed]` and add things here you want to disable temporarily
                - I use this functionality to destroy items when I'm playing with others who don't use a loot macro so it's easier for them to find useful stuff, and I disable it when not needed.
			5. **ANNOUNCE** This is contained in cMac.ini -- In the `[AnnounceLoot]` section you can put: `A Glowing Hammer Handle=Announce`
                - This will announce a hammer handle
                - in cMac.ini there is a `[cLoot]` section which has settings for looting including these two related to announce
					- MasterAnnouncer - Set this to the character you want to announce items in your announce list
					- MassAnnounce - Set to 1 if you want ALL characters to announce, or 0 to only announce with the masterannouncer
7. **cMac.ini** - General settings that the whole system will pull from
	1. BroadcastPlugin - If you use DanNet, you can leave this as default, if not and you use EQBC do this command once you load cMac: `/cEQBC`
	2. UseZoneBroadcasting - This is useful if you have characters logged in who are in different zones. If you have DanNet, you have it, if EQBC you need an extra plugin
	3. MainTankName - Set this to your MT's name
	4. `[cInstCharacters]` - If you have Free Waypoint item and are going to use cInstance add the characters who have them here.


## Feature List

 **All commonly required macros and QoL stuff for boxing**  


  1. My system allows for any of your characters to drive using the exact same UI and hotkeys
  2. `cMain cAssist`
  3. `cMain cFollow`
  4. `cSpam` - The first of my macros that is customizable on a per class basis. You can put anything you want here that you spam in combat Warriors will keep their epic up, rogues will backstab, casters can cast their spells, etc
  5. `cMain cStop` - This will tell all your boxes to stop following you
  
  
 **Extended combat functionality**  
  1. **Healing** Important - The healing commands get sent to the "healer" channel.
				 You will need to add your healers to this channel, look for channel reference in the EQBC or DanNet Wikis
				 Then, you will need to make sure cHeal.inc contains the proper spells for each class that you have at the time.
      1. `cMain cHealMe` - Your command group healer team will heal the driver (who pressed the button)
      2. `cMain cHealTarget` - Your healers will heal your target
      3. `cHeal true` - This is a macro your put on your healers and it will perform their Group heal
      4. `cHeal false` - This is a macro your put on your healers and it will perform their Single Target heal
  2. `/dex Jeshetiq cVie ${Me.Name}` - This will tell my cleric (Jeshetiq) to cast any memmed and ready version of Vie on the driver.
  3. `/dex ClydeDruid cDrake ${Me.ID}` - This will tell my druid (ClydeDruid) to cast any memmed and ready version of Drake on the driver.
  4. `/dex Jeshetiq cMark ${Target.ID}` - This will tell my cleric (Jeshetiq) to cast any memmed and ready version of Mark of Emperors on your target.
  5. `cOffensive` - On a per-class basis, it will cast their DPS burn spells/aas/discs
  6. `cDefensive` - On a per-class basis, it will cast their Defensive type spells/aas/discs
  7. `cEmissary` - Will Use all boss crystals in Plane of Dragons when you are near an Emissary (use this on a box, not your driver)
  8. `cEoN` - Will make Essence of Norraths for you if you have components!
  9. `cHoHTok` - Will combine tokens from HoH to create T4 tokens (caution will use ALL avaialbe tokens on character, if you want to keep some T3 put them in the bank first)
  10. `cSoa` - Put your Shield of ages on your cursor, and have mats in your inventory with Bag10 empty.
  
  
 3. **Extra Utilities**
    1. `cLoot ${Me.Name}` - This will tell the button-presser to loot based on the turboloot INI for your character
    2. `cBuff` - On a class based basis it is set up to cast their buffs with MGB if you want even. All buffs can be done for your whole team in seconds
    3. `cStoneConvert` - This will convert your Heroic Resistance stones to the higher ranks based on configuration in the file.
    4. `cMain Gobbie` - I have this set up to click off Angryface Familiar, cast Insane Goblin Familiar, and then tell the pet to leave
    5. `cMain AngryFace` - I have this set up to click off Insane Goblin Familiar, cast AngryFace Familiar, and then tell the pet to leave
    6. `cAirClick` - Will click AirPlane T3/T4 tokens until you run out. It should wait until a boss is dead before using a token, if it wastes a token it should do a /rs panic and end the macro (use at your own risk)
    7. `cBuyAA` - Will buy all available AAs

 4. **Instance Management** (EZ Server Instancing System) - Danger: use this if you have the free waypoint reward item, otherwise you'll use a lot of plat!
    1. `cI Cre` - Will create an instances of your current zone. Both Guild and Raid by all characters specified in "cInstance.ini"
    2. `cI Cre FrozenShadow` - Similar to the above command, but for creating remote instances. This example will create instances for Frozenshadow
    3. `cI Ent` - In your "cInstance.ini" there is a CurrentInstance variable with a number assigned to it, when you do this command it will go to the next instance in the list
    4. `cI Ent 3` - In your "cInstance.ini" this will go to the instance assigned to "3"
    5. `cI Add enter double qekiibre qvic` - This will add exactly "enter double qekiibre qvic" to your instance list, so if somebody else creates an instance for you this is how you add it to the macro
    6. `cI Rem 3` - This will set the instance in slot "3" to null... It can't be deleted, but this will make it easy to see which one you need to remove manually from the INI file
    7. `cI List` - This will list all instances stored in the macro with /echo
  
 5. **cCommonUtils** (MacroMaking and modifying stuff)
    1. `sub cCastProperSpellRank` - This is to make it easier for new players to set up their macros to cast things for them.
    2. MGB - can set to use MGB with a cast
    3. Post Cast Delay - Useful for Buff sequences where you want to make sure all spells get cast in a row
    4. Automatic Spell Rank Detection - just do `/call cCastProperSpellRank "Howl of the Huntmaster"` and it will cast the proper rank that you have memmed
  

 6. **EZ Progression Tracker**
    1. `/dgae /echo cProg t8` - Will list all your characters needed item to complete T8
    2. `/dgae /echo cProgs t8 head` - Will list all your characters head items needed item to complete T8
    3. `cProg help` - See some info about the macro


### TODO
	- General
	 - **TESTING**
	  - ~~Set up a RoF client + MQ2 from the wiki and MQ2 from ( https://mqemulator.net/ )~~
	   - New players will likely start with this instead of the RG MQ2 compile,
		 so I need to see if there's RG specific stuff I'm using
	 - ~~Create Global settings INI file~~
	  -	~~Have translation for DanNet / EQBCS be an option~~
	  
	  
	 - cLoot
	  -	**Debug issues**
	   - ~~Was doing Airplane T3 and it was leaving some Lightstones for some reason~~
	   - ~~Issues of Trying to use a named "cTurboLoot_Anguish.ini" ini file for Anguish progression specifically and it wasn't working properly.~~
	    - ~~Note: My original intention of my implementation was 1 ini per character, so I'm not too surprised it's having issues when I'm trying to use it like this~~
	   
	   
	- cInstance
	 - **Debug issues**
	 - ~~Add check for in comabt message and decrement counter~~
	 - If an instance on the list expires, and you use /echo ci cre to recreate it before removing from INI it gets added to the tail. Need to check if it already exists!
	 
	 
	- cStoneConvert
	 - ~~Parameterize either through INI, or through the echo call~~
	   ~~so that it can be changed on the fly without going into the file~~
	 - 	Generisize it so it can do other gem types too
	 
	 
	- cMark
     - ~~Has an issue with the if statements to not cast if it has a lower rank.~~
		
		
		
		
		
		
		
		
		
		
#### CREDITS - These are macros that I found from others and modified to fit my needs
cMac.mac-					- [Dimur](http://ezserver.online/forums/index.php?topic=5731.0)  
cHoHTokenCombine.inc 		- [Knorgar](http://ezserver.online/forums/index.php?topic=5167.15)  
cBuyAA.inc 					- [Natedog](http://ezserver.online/forums/index.php?topic=5167.15)  
cEmissary.inc 				- [s0rcier](http://ezserver.online/forums/index.php?topic=5603.0)  
cAirClick.inc 				- [s0rcier](http://ezserver.online/forums/index.php?topic=5621)  
cEZProgression.inc 			- [Ogru](http://ezserver.online/forums/index.php?topic=6089.0)  
cTurboLoot.inc 				- [bruise](http://ezserver.online/forums/index.php?topic=6207.0)  
There are others I may have left out, sorry if so, but this is really a community effort so thanks everybody!