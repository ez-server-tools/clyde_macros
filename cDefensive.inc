#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.


#Event cDefensive	"cDefensive"


|==== Sub Event_cDefensive
	Sub Event_cDefensive
		/call cDefensiveLogic
	/return
|==== End Sub Event_cDefensive


Sub cDefensiveLogic
	/if (!${Defined[MF_BuffTicks]}) /declare MF_BuffTicks ticks global 60000
	

	/if (${Me.Class.Name.Equal[Warrior]}) {
		/call DefensiveWarrior
	}
	/if (${Me.Class.Name.Equal[Berserker]}) {
		/call DefensiveBerserker
	}
	/if (${Me.Class.Name.Equal[Shaman]}) {
		/call DefensiveShaman
	}
	/if (${Me.Class.Name.Equal[Cleric]}) {
		/call DefensiveCleric
	}
	/if (${Me.Class.Name.Equal[Bard]}) {
		/call DefensiveBard
	}
	/if (${Me.Class.Name.Equal[Paladin]}) {
		/call DefensivePaladin
	}
	/if (${Me.Class.Name.Equal[Druid]}) {
		/call DefensiveDruid
	}
	/if (${Me.Class.Name.Equal[Rogue]}) {
		/call DefensiveRogue
	}
	/if (${Me.Class.Name.Equal[Ranger]}) {
		/call DefensiveRanger
	}
	/if (${Me.Class.Name.Equal[Necromancer]}) {
		/call DefensiveNecromancer
	}
/return

|-------------

Sub DefensiveWarrior

	/if (${Me.CombatAbilityReady[Evasive Discipline]}) {
		/disc Evasive Discipline
	}
	
	
/return


Sub DefensiveBerserker

	/if (${Me.AltAbilityReady[Uncanny Resilience]}) {
		/casting "Uncanny Resilience"
	}

/return


Sub DefensiveShaman

	/if (${Cast.Ready[Ancestral Aid]}) /casting "Ancestral Aid"
	/if (${Cast.Ready[Ancestral Guard]}) /casting "Ancestral Guard"
	
/return


Sub DefensiveCleric

	/if (${Cast.Ready[Celestial Regeneration]}) /casting "Celestial Regeneration"
	/if (${Cast.Ready[Divine Arbitration]}) /casting "Divine Arbitration"
	/if (${Cast.Ready[Divine Retribution]}) /casting "Divine Retribution"
	/if (${Cast.Ready[Exquisite Benediction]}) /casting "Exquisite Benediction"
	/if (${Cast.Ready[Sanctuary]}) /casting "Sanctuary"
	
/return


Sub DefensiveBard
	
	/if (${Me.CombatAbilityReady[deftdance discipline]}) {
		/disc deftdance discipline
	}
	/if (${Cast.Ready[Shield of Notes]}) /casting "Shield of Notes"
	/if (${Cast.Ready[Hymn of the Last Stand]}) /casting "Hymn of the Last Stand"
	
/return


Sub DefensivePaladin

	/if (${Me.CombatAbilityReady[Guard of Righteouness]}) {
		/disc Guard of Righteouness
	}
	/if (${Me.CombatAbilityReady[Sanctification Discipline]}) {
		/disc Sanctification Discipline
	}
	/if (${Cast.Ready[Armor of the Inquisitor]}) /casting "Armor of the Inquisitor"
	/if (${Cast.Ready[Hand of Piety]}) /casting "Hand of Piety"
	
/return


Sub DefensiveDruid


	
	
/return


Sub DefensiveRogue


	
	
/return


Sub DefensiveRanger

	/if (${Me.CombatAbilityReady[Weapon Shield Discipline]}) {
		/disc Weapon Shield Discipline
	}
	
/return


Sub DefensiveNecromancer


	
	
/return

|----------