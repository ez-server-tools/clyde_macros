#warning


|====== Modified by Clyde for EZ Server to fit my cMacro echo system
|====== Original credit: s0rcier - http://ezserver.online/forums/index.php?topic=5603.0
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|======
|======
|====== This is for use in Plane of Dragons at the Emissaries to turn in crystals until you are out.
|====== Use this on an alt, not your driver.


#Event cEmissary	"cEmissary"


|==== Sub Event_cEmissary
	Sub Event_cEmissary
		/call cEmissaryLogic
	/return
|==== End Sub Event_cEmissary


Sub cEmissaryLogic
    /declare n string local
	/declare c int local
	
	:again
	/delay 1
	/doevents
    /if (${Cursor.ID}) /autoinv
	/if (${SpawnCount[npc Emissary radius 50]}<1) /goto :again
	/squelch /target Emissary
	/delay 2s ${Target.CleanName.Find[Emissary]}
	/delay 2s !${Cursor.ID}
	
	/if (${Target.CleanName.Find[Ice]}) /varset n Ice Crystal
	/if (${Target.CleanName.Find[Fire]}) /varset n Fire Crystal
	/if (${Target.CleanName.Find[Earth]}) /varset n Earth Crystal
	/if (${Target.CleanName.Find[Water]}) /varset n Water Crystal
	
    /varcalc c ${FindItemCount[=${n}]}

    /if (${c}>0) {
        /echo emissary.mac Found:: ${n}=[x${c}]
	/if (!${Defined[BagNum]}) /declare BagNum int local
	/varcalc BagNum ${FindItem[=${n}].ItemSlot}
	/varcalc BagNum ${BagNum} - 22
	/if (!${Defined[BagSlot]}) /declare BagSlot int local
	/varcalc BagSlot ${FindItem[=${n}].ItemSlot2}
	/varcalc BagSlot ${BagSlot} +1
	/ctrl /itemnotify in pack${BagNum} ${BagSlot} leftmouseup

        /delay 5s ${Cursor.ID}
        /click left target
        /delay 5s !${Cursor.ID}
	
        /delay 2s ${Window[GiveWnd].Open}
			
        /if (${Window[GiveWnd].Open}) {
            /notify GiveWnd GVW_Give_Button LeftMouseUP
            /delay 5s !${Window[GiveWnd].Open}
            /goto :again
        }
	}
	/goto :again
    	
	/if (${Plugin[MQ2EQBC].Version}) /bcaa //echo cEmissary Done
	/if (${Plugin[MQ2DanNet].Version}) /dgae /echo cEmissary Done

/return


