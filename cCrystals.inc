#warning


|====== Modified by Clyde for EZ Server to fit my cMacro echo system
|====== Original credit: Not sure, need to find
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|======
|112921 - 112927 - T3 IDs
|112931 - 112937 - T4 IDs

#Event cCrystals	"cCrystals"

|==== Sub Event_cCrystals
	Sub Event_cCrystals
		/call cCrystalsLogic
	/return
|==== End Sub Event_cCrystals


Sub cCrystalsLogic
    /declare crys1a int local 
    /declare crys1b int local 
    /declare crys2a int local 
    /declare crys2b int local 
    
    /tar miner
    /delay 1s
    
    :Start 
        /if (!${FindItem[132065].ID}==NULL && ${FindItemCount[132065]}>1)  {
            /varset crys1a ${Int[${Math.Calc[${FindItem[132065].ItemSlot}-22]}]}
            /varset crys1b ${Int[${Math.Calc[${FindItem[132065].ItemSlot2}+1]}]}
            
            /ctrl /itemnotify in pack${crys1a} ${crys1b} leftmouseup
            /delay 5
            /click left target
            /delay 5
            /varset crys1a ${Int[${Math.Calc[${FindItem[132065].ItemSlot}-22]}]}
            /varset crys1b ${Int[${Math.Calc[${FindItem[132065].ItemSlot2}+1]}]}
            /ctrl /itemnotify in pack${crys1a} ${crys1b} leftmouseup
            /click left target
            /delay 5
            /if (${Window[GiveWnd].Open} && !${Cursor.ID}) {
                /notify GiveWnd GVW_Give_Button leftmouseup
            }
            /delay 5
			/if (${Cursor.ID}==132067) {
				/echo Success 20k Crystal
			} else {
				/echo Lesser Crystal Failure
			}
            /autoinventory
            /goto :Start
        }
        
        /if (!${FindItem[132064].ID}==NULL && ${FindItemCount[132064]}>1)  {
            /varset crys1a ${Int[${Math.Calc[${FindItem[132064].ItemSlot}-22]}]}
            /varset crys1b ${Int[${Math.Calc[${FindItem[132064].ItemSlot2}+1]}]}
            
            /ctrl /itemnotify in pack${crys1a} ${crys1b} leftmouseup
            /delay 5
            /click left target
            /delay 5
            /varset crys1a ${Int[${Math.Calc[${FindItem[132064].ItemSlot}-22]}]}
            /varset crys1b ${Int[${Math.Calc[${FindItem[132064].ItemSlot2}+1]}]}
            /ctrl /itemnotify in pack${crys1a} ${crys1b} leftmouseup
            /click left target
            /delay 5
            /if (${Window[GiveWnd].Open} && !${Cursor.ID}) {
                /notify GiveWnd GVW_Give_Button leftmouseup
            }
            /delay 5
			/if (${Cursor.ID}==132066) {
				/echo Success 10k Crystal
			} else {
				/echo Minor Crystal Failure
			}
            /autoinventory
            /goto :Start
        }
        /tar keeper
        /delay 5
        /stick 10
        /delay 100
        /stick off
        
    :giveCrystals
    
        /if (${FindItemCount[132066]}>0) {
            /varset crys1a ${Int[${Math.Calc[${FindItem[132066].ItemSlot}-22]}]}
            /varset crys1b ${Int[${Math.Calc[${FindItem[132066].ItemSlot2}+1]}]}
            /ctrl /itemnotify in pack${crys1a} ${crys1b} leftmouseup
            /delay 6
            /click left target
            /delay 6
            
            /if (${Window[GiveWnd].Open} && !${Cursor.ID}) {
                /notify GiveWnd GVW_Give_Button leftmouseup
            }
            /delay 5
            /say #stats
            /goto :giveCrystals
    }
	
		/if (${FindItemCount[132067]}>0) {
			/varset crys1a ${Int[${Math.Calc[${FindItem[132067].ItemSlot}-22]}]}
			/varset crys1b ${Int[${Math.Calc[${FindItem[132067].ItemSlot2}+1]}]}
			/ctrl /itemnotify in pack${crys1a} ${crys1b} leftmouseup
			/delay 6
			/click left target
			/delay 6
		
			/if (${Window[GiveWnd].Open} && !${Cursor.ID}) {
				/notify GiveWnd GVW_Give_Button leftmouseup
			}
			/delay 5
			/say #stats
			/goto :giveCrystals
    }
    
    /return