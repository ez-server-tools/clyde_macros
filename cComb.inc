#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.


#Event cComb1		"cComb #1#"
#Event cComb		"cComb"


|==== Sub Event_cComb
	Sub Event_cComb1(Line, cCombRecipe)
		/call cCombLogic ${cCombRecipe}
	/return
	Sub Event_cComb
		/if (!${Cursor.ID}) {
			/echo Ending unsuccessfully - Need an item on cursor if you don't specify recipe name
			/return
		}
	
		/call cCombLogic
	/return
|==== End Sub Event_cComb

Sub cCombLogic(cCombRecipe)
	/if (!${Defined[cCombIni]}) 			/declare cCombIni				string	local cMacros\cComb.ini
	/if (!${Defined[cRecipe]}) 				/declare cRecipeName			string	local
	/if (!${Defined[cItemName_cComb]}) 		/declare cItemName_cComb		string	outer
	/if (!${Defined[cIngredientCount]}) 	/declare cIngredientCount		int		local
	/if (!${Defined[cIngredientIter]}) 		/declare cIngredientIter		int		local
	/if (!${Defined[cIngredientInnerIter]}) /declare cIngredientInnerIter	int		local
	/if (!${Defined[cIngredientAmount]}) 	/declare cIngredientAmount		int		local
	/if (!${Defined[cEndFlag]}) 			/declare cEndFlag				int		local 0
	/if (!${Defined[cAmountHave]}) 			/declare cAmountHave			int		local
	/if (!${Defined[cPutItemInSlot]}) 		/declare cPutItemInSlot			int		local 1	
	
	/if (${cCombRecipe.Length}>0) {
		/varset cRecipeName ${cCombRecipe}
	} else {
		/varset cRecipeName ${Cursor.Name}
	}
	
	/if (${Ini[${cCombIni},${cRecipeName}].Length} < 1) {
		/echo Ending unsuccessfully - No Recipe Defined in INI
		/return
	}
	
	/autoi
	/delay 1m !${Cursor.ID}
	
	/varset cIngredientCount ${Math.Calc[${Ini[${cCombIni},${cRecipeName}].Count[|]}-1]}
	
	/for cIngredientIter 1 to ${cIngredientCount} {
		/varset cIngredientAmount ${Ini[${cCombIni},${cRecipeName},${cIngredientIter}].Token[2,,]}
		/varset cAmountHave ${FindItemCount[${Ini[${cCombIni},${cRecipeName},${cIngredientIter}].Token[1,,]}]}
		/if (${cAmountHave} < ${cIngredientAmount}) {
			/varset cEndFlag 1
			/echo Not Enough of Item: ${Ini[${cCombIni},${cRecipeName},${cIngredientIter}].Token[1,,]} - Need ${cIngredientAmount}, have ${cAmountHave}
		}
	
		/next cIngredientIter
	}
	
	/if (${cEndFlag}) {
		/echo Ending unsuccessfully - Missing Ingredients
		/return
	}
	
	
	/for cIngredientIter 1 to ${cIngredientCount} {
		/varset cItemName_cComb ${Ini[${cCombIni},${cRecipeName},${cIngredientIter}].Token[1,,]}
		/varset cIngredientAmount ${Ini[${cCombIni},${cRecipeName},${cIngredientIter}].Token[2,,]}
		
		
		/for cIngredientInnerIter 1 to ${cIngredientAmount} {
			/call PutItemInBag_cComb ${cPutItemInSlot}
			/varcalc cPutItemInSlot ${cPutItemInSlot}+1
		
			/next cIngredientInnerIter
		}
	
		/next cIngredientIter
	}
	
	/itemnotify pack10 rightmouseup
	/delay 1m ${Window[pack10].Open}
	/combine pack10
	/delay 1m ${Cursor.ID}
	/itemnotify pack10 rightmouseup
	/delay 1m !${Window[pack10].Open}
	
	
	/echo Ending successfully
/return







Sub PutItemInBag_cComb(int BagSlot)

	/if (!${Defined[BagNum]}) /declare BagNum int local
	/varcalc BagNum ${FindItem[=${cItemName_cComb}].ItemSlot}
	/varcalc BagNum ${BagNum}-22
	/if (!${Defined[ItemBagSlot]}) /declare ItemBagSlot int local
	/varcalc ItemBagSlot ${FindItem[=${cItemName_cComb}].ItemSlot2}
	/varcalc ItemBagSlot ${ItemBagSlot}+1
	
	/nomodkey /ctrlkey /itemnotify in pack${BagNum} ${ItemBagSlot} leftmouseup
	/delay 1m ${Cursor.ID} == ${FindItem[=${cItemName_cComb}].ID}
	/itemnotify in pack10 ${BagSlot} leftmouseup
	/delay 1m !${Cursor.ID}
/return