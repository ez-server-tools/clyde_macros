#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.


#Event cHeal		"cHeal #1#"


|==== Sub Event_cHeal
	Sub Event_cHeal(Line, IsAE)
		/call cHealLogic ${IsAE}
	/return
|==== End Sub Event_cHeal


Sub cHealLogic(IsAE)
	/if (!${Defined[MF_BuffTicks]}) /declare MF_BuffTicks ticks global 60000


	/if (${Me.Class.Name.Equal[Warrior]}) {
		/call HealWarrior ${IsAE.Equal[true]}
	}
	/if (${Me.Class.Name.Equal[Berserker]}) {
		/call HealBerserker ${IsAE.Equal[true]}
	}
	/if (${Me.Class.Name.Equal[Shaman]}) {
		/call HealShaman ${IsAE.Equal[true]}
	}
	/if (${Me.Class.Name.Equal[Cleric]}) {
		/call HealCleric ${IsAE.Equal[true]}
	}
	/if (${Me.Class.Name.Equal[Bard]}) {
		/call HealBard ${IsAE.Equal[true]}
	}
	/if (${Me.Class.Name.Equal[Paladin]}) {
		/call HealPaladin ${IsAE.Equal[true]}
	}
	/if (${Me.Class.Name.Equal[Druid]}) {
		/call HealDruid ${IsAE.Equal[true]}
	}
	/if (${Me.Class.Name.Equal[Rogue]}) {
		/call HealRogue ${IsAE.Equal[true]}
	}
	/if (${Me.Class.Name.Equal[Necromancer]}) {
		/call HealNecromancer ${IsAE.Equal[true]}
	}
/return

|-------------

Sub HealWarrior(IsAE)

	
	
/return


Sub HealBerserker(IsAE)
	
	
	
/return


Sub HealShaman(IsAE)
	
	/if (${IsAE.Equal[true]}) {
		/call cCastProperSpellRank "Breath of Nature"
	}
	
	/if (${IsAE.Equal[false]}) {
		/if (${Cast.Ready[Ancient: Chlorobon]}) /casting "Ancient: Chlorobon"
	}
	
/return


Sub HealCleric(IsAE)

	/if (${IsAE.Equal[true]}) {
		/multiline ; /call cCastProperSpellRank "Word of Vivification"
		/delay 2
		/itemnotify 13 rightmouseup
	}
	
	/if (${IsAE.Equal[false]}) {
		/multiline  ; /call cCastProperSpellRank "Over Raided Healing"
	}
	
/return


Sub HealBard(IsAE)
	
	
	
/return


Sub HealPaladin(IsAE)

	/if (${IsAE.Equal[true]}) {
		/multiline ; /if (${Cast.Ready[Crabtwoshoes Will Heal You Too!]}) /casting "Crabtwoshoes Will Heal You Too!" ; /if (${Cast.Ready[Crabtwoshoes Will Heal You!]}) /casting "Crabtwoshoes Will Heal You!"
	}
	
	/if (${IsAE.Equal[false]}) {
		/multiline ; /if (${Cast.Ready[Crabtwoshoes Will Heal You Too!]}) /casting "Crabtwoshoes Will Heal You Too!" ; /if (${Cast.Ready[Crabtwoshoes Will Heal You!]}) /casting "Crabtwoshoes Will Heal You!"
	}	
	
/return


Sub HealDruid(IsAE)

	/if (${IsAE.Equal[true]}) {
		/call cCastProperSpellRank "Breath of Nature"
	}
	
	/if (${IsAE.Equal[false]}) {
		/multiline ; /if (${Cast.Ready[Complete Healing]}) /casting "Complete Healing" ; /call cCastProperSpellRank "Timeless: Chlorobon"
	}
	
/return


Sub HealRogue(IsAE)


	
	
/return


Sub HealNecromancer(IsAE)


	
	
/return

|----------