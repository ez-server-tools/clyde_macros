#warning


|====== Modified by Clyde for EZ Server to fit my cMacro echo system
|====== Original credit: Natedog - Need to find link
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|======
|======
|====== This is for use in Plane of Dragons at the Emissaries to turn in crystals until you are out.
|====== Use this on an alt, not your driver.


#Event cSell		"cSell"



|==== Sub Event_cSell
	Sub Event_cSell
		/call cSellLogic
	/return
|==== End Sub Event_cSell

Sub cSellLogic
/call SETUPLOOT
/call PackSell
/return


sub SETUPLOOT
	/if (!${Ini[cMacros\cSell.ini,Setup,Version]})	/call LoadLoot
/return

sub LoadLoot
	/ini "cMacros\cSell.ini" "Setup" "Version" "1"
	/ini "cMacros\cSell.ini" "A" "1" "1"
	/ini "cMacros\cSell.ini" "B" "1" "1"
	/ini "cMacros\cSell.ini" "C" "1" "1"
	/ini "cMacros\cSell.ini" "D" "1" "1"
	/ini "cMacros\cSell.ini" "E" "1" "1"
	/ini "cMacros\cSell.ini" "F" "1" "1"
	/ini "cMacros\cSell.ini" "G" "1" "1"
	/ini "cMacros\cSell.ini" "H" "1" "1"
	/ini "cMacros\cSell.ini" "I" "1" "1"
	/ini "cMacros\cSell.ini" "J" "1" "1"
	/ini "cMacros\cSell.ini" "K" "1" "1"
	/ini "cMacros\cSell.ini" "L" "1" "1"
	/ini "cMacros\cSell.ini" "M" "1" "1"
	/ini "cMacros\cSell.ini" "N" "1" "1"
	/ini "cMacros\cSell.ini" "O" "1" "1"
	/ini "cMacros\cSell.ini" "P" "1" "1"
	/ini "cMacros\cSell.ini" "Q" "1" "1"
	/ini "cMacros\cSell.ini" "R" "1" "1"
	/ini "cMacros\cSell.ini" "S" "1" "1"
	/ini "cMacros\cSell.ini" "T" "1" "1"
	/ini "cMacros\cSell.ini" "U" "1" "1"
	/ini "cMacros\cSell.ini" "V" "1" "1"
	/ini "cMacros\cSell.ini" "W" "1" "1"
	/ini "cMacros\cSell.ini" "X" "1" "1"
	/ini "cMacros\cSell.ini" "Y" "1" "1"
	/ini "cMacros\cSell.ini" "Z" "1" "1"
/return


sub PackSell
	/echo Starting Sell Routine
	/if (!${Window[MerchantWnd].Open}) {
		/echo Merchant window not open, attempting to interact with target.
		/nomodkey /click right target
		/delay 5s ${Window[MerchantWnd].Open}
	}

	/if (${Window[MerchantWnd].Open}) {
		/declare packnum int local
		/declare T int local
		/declare RealPackNum int local
		
		/for packnum 1 to 10
			/varcalc RealPackNum ${packnum}+22
			/if (!${Window[pack${packnum}].Open} && ${InvSlot[pack${packnum}].Item.Container}) {
				/itemnotify pack${packnum} rightmouseup
				/delay 4
			}
			/if (${Window[pack${packnum}].Open} && ${InvSlot[pack${packnum}].Item.Container}) {
				/for T 1 to 10
					/if (${Ini[cMacros\cSell.ini,${InvSlot[pack${packnum}].Item.Item[${T}].Name.Left[1]},${InvSlot[pack${packnum}].Item.Item[${T}].Name}].Length}) {
						/if (${Ini[cMacros\cSell.ini,${InvSlot[pack${packnum}].Item.Item[${T}].Name.Left[1]},${InvSlot[pack${packnum}].Item.Item[${T}].Name}].Equal[SELL]}) {
							/echo Sell: pack${packnum}:${T} -- ${InvSlot[pack${packnum}].Item.Item[${T}].Name.Left[1]}
							/itemnotify in pack${packnum} ${T} leftmouseup
							/delay 4
							/shiftkey /notify MerchantWnd MW_Sell_Button leftmouseup
							/delay 4
						}
					} else {
						/if (${InvSlot[pack${packnum}].Item.Item[${T}].SellPrice} > 0 && !${InvSlot[pack${packnum}].Item.Item[${T}].NoDrop} && !${InvSlot[pack${packnum}].Item.Item[${T}].NoRent}) {
						
							/ini "cMacros\cSell.ini" "${InvSlot[pack${packnum}].Item.Item[${T}].Name.Left[1]}" "${InvSlot[pack${packnum}].Item.Item[${T}].Name}" "VALUED"
						}
						/if (${InvSlot[pack${packnum}].Item.Item[${T}].NoDrop} && !${InvSlot[pack${packnum}].Item.Item[${T}].NoRent}) {
						
							/ini "cMacros\cSell.ini" "${InvSlot[pack${packnum}].Item.Item[${T}].Name.Left[1]}" "${InvSlot[pack${packnum}].Item.Item[${T}].Name}" "ND"
						}
						/if (${InvSlot[pack${packnum}].Item.Item[${T}].SellPrice} == 0 !${InvSlot[pack${packnum}].Item.Item[${T}].NoDrop} && !${InvSlot[pack${packnum}].Item.Item[${T}].NoRent}) {
						
							/ini "cMacros\cSell.ini" "${InvSlot[pack${packnum}].Item.Item[${T}].Name.Left[1]}" "${InvSlot[pack${packnum}].Item.Item[${T}].Name}" "NOVALUE"
						}
					|SellPrice
					}
				/next T
			}
			/if (${Window[pack${packnum}].Open} && ${InvSlot[pack${packnum}].Item.Container}) /itemnotify pack${packnum} rightmouseup
		/next packnum
	}
/return