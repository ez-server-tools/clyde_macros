#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.


#Event cSpam		"cSpam"


|==== Sub Event_cSpam
	Sub Event_cSpam
		/call cSpamLogic
	/return
|==== End Sub Event_cSpam


Sub cSpamLogic	
	/if (!${Defined[MF_BuffTicks]}) /declare MF_BuffTicks ticks global 5000000


	/if (${Me.Inventory[Back].Name.Find[Shield of the Ages]} && !${FindItem[Shield of the Ages].Timer} && !${Me.Buff[${FindItem[Shield of the Ages].Spell}].ID}) {
        /nomodkey /itemnotify back rightmouseup
    }


	| Requires Plugin MQ2Debuffs for Debuff.Detrimentals
	/if (${Debuff.Detrimentals} && ${FindItem[Instant Cure Group].ID} && !${FindItem[Instant Cure Group].Timer} && !${Me.Buff[Enrage].ID}) {
		/casting "${FindItem[Instant Cure Group]}"
	}

	/if (${Me.Class.Name.Equal[Warrior]}) {
		/call SpamWarrior
	}
	/if (${Me.Class.Name.Equal[Berserker]}) {
		/call SpamBerserker
	}
	/if (${Me.Class.Name.Equal[Shaman]}) {
		/call SpamShaman
	}
	/if (${Me.Class.Name.Equal[Cleric]}) {
		/call SpamCleric
	}
	/if (${Me.Class.Name.Equal[Bard]}) {
		/call SpamBard
	}
	/if (${Me.Class.Name.Equal[Paladin]}) {
		/call SpamPaladin
	}
	/if (${Me.Class.Name.Equal[Druid]}) {
		/call SpamDruid
	}
	/if (${Me.Class.Name.Equal[Rogue]}) {
		/call SpamRogue
	}
	/if (${Me.Class.Name.Equal[Ranger]}) {
		/call SpamRanger
	}
	/if (${Me.Class.Name.Equal[Necromancer]}) {
		/call SpamNecromancer
	}
	/if (${Me.Class.Name.Equal[Magician]}) {
		/call SpamMagician
	}}
	/if (${Me.Class.Name.Equal[Beastlord]}) {
		/call SpamBeastlord
	}
	/if (${Me.Class.Name.Equal[Wizard]}) {
		/call SpamWizard
	}
	/if (${Me.Class.Name.Equal[Monk]}) {
		/call SpamMonk
	}
/return

|-------------

Sub SpamWarrior

	/if (${Me.Buff[Warrior's Defense].Duration} < 80000) {
		/itemnotify 13 rightmouseup
	}
	
	/if (${Me.Buff[Ancient Stonewall].Duration} < 60000) {
		/itemnotify 8 rightmouseup
	}
	
	/if (${Ini[${cMacIni},Settings,MainTankName].Equal[${Me.Name}]}) {
		/if (${FindItem[Sceptre of Time I].ID}) /itemnotify  "Sceptre of Time I" rightmouseup
		/if (${FindItem[Warrior Class Augment 3.0].ID}) /itemnotify "Warrior Class Augment 3.0" rightmouseup
		/if (${FindItem[Time Rift Power Generator].ID}) /itemnotify "Time Rift Power Generator" rightmouseup
	
		/if (${Me.AbilityReady[Taunt]}) /doability Taunt
		/if (!${FindItem[Bashful Crustaceans' Angerbomb].TimerReady}) {
			/itemnotify  "Bashful Crustaceans' Angerbomb" rightmouseup
		}
		/if (${FindItem[Sceptre of Time I].ID}) {
			/if (!${FindItem[Sceptre of Time I].TimerReady}) {
				/itemnotify  "Sceptre of Time I" rightmouseup
			}
		}
		
		/if (${Me.CombatAbilityReady[Bazu Bellow]}) {
			/discipline Bazu Bellow
		}
		/if (${Me.CombatAbilityReady[Infallible]}) {
			/discipline Infallible
		}
	}
	
	/if (${Me.AbilityReady[Slam]}) /doability Slam
	
	
	
	
	
	
	
	/if (!${Me.Song[Commanding Voice].ID}) {
		/discipline Commanding Voice
	}
	
	/if (${Me.CombatAbilityReady[Cyclone Blade]}) {
		/discipline Cyclone Blade
	}
	
/return


Sub SpamBerserker

	/if (${Me.AbilityReady[Frenzy]}) /doability Frenzy

	/if (${Me.Buff[Over Raided Zerker Haste].Duration} < 60000) {
		/casting "Over Raided Zerker Haste" item -maxtries|1
	}
	
	/if (${FindItem[Bashful Crustaceans' Pincer Attack].TimerReady}==0) {
		/casting "Bashful Crustaceans' Pincer Attack" item -maxtries|1
	}
	
	|/if (${FindItem[Wrathbringer's Chain Chestguard of the Vindicator].TimerReady}==0) {
	|	/casting "Wrathbringer's Chain Chestguard of the Vindicator" item -maxtries|1
	|}
	/if (${Me.CombatAbilityReady[Destroyer's Volley]}) {
		/disc Destroyer's Volley
	}
	/if (!${Me.Song[Cry Havoc].ID}) {
		/casting cry havoc -maxtries|1
	}
	/if (${Me.AltAbilityReady[Rampage]}) {
		/casting "rampage"
	}
	
	| Removing because it removed effect from Pincer Attack Air item
	|/if (${Me.AltAbilityReady[Blood Pact]}) {
	|	/casting "Blood Pact"
	|}
	/if (${Me.AltAbilityReady[Cascading Rage]}) {
		/casting "Cascading Rage"
	}
	/if (${Me.AltAbilityReady[Blinding Fury]}) {
		/casting "Blinding Fury"
	}
	/if (${Me.AltAbilityReady[Desperation]}) {
		/casting "Desperation"
	}
	/if (${Me.AltAbilityReady[Savage Spirit]}) {
		/casting "Savage Spirit"
	}
	
	/itemnotify 13 rightmouseup
	
	|/if (!${Defined[EpicTimer]}) /declare EpicTimer timer local
	|/echo ${EpicTimer}
	|/if (${EpicTimer}==0) {
	|	/casting "Berserker Class Augment 4.0" item -maxtries|1
	|	/varset EpicTimer 60
	|}
	
/return


Sub SpamShaman

	/if (!${Defined[MainAssist]}) 	/declare MainAssist 	string local Qekiibre
	/if (!${Defined[Group1Leader]}) /declare Group1Leader	string local Qekiibre
	/if (!${Defined[Group2Leader]}) /declare Group2Leader	string local ClydeRanger

	|/if (!${Me.Buff[Focus of Healing].ID}) {
	|	/casting "Focus of Healing" gem3 -maxtries|1
	|}

	/if (!${Me.Buff[Wisest Healer].ID}) {
		/casting "Crystalline Idol of Healing Boon"
	}	
	
	/if (${Cast.Ready[Spirit of the Kraken II]}) {	
		/if (${Me.Buff[Spirit of the Kraken].Duration} < 80000) {
			/tar pc ${Group2Leader}
			/delay 6
			/if (!${Target.Buff[Spirit of the Kraken].ID}) {
				/call cCastProperSpellRank "Spirit of the Kraken"
				/delay 6
				/removebuff "Spirit of the Kraken"
				/delay 6
				/assist ${MainAssist}
			} else {
				/tar pc ${Group1Leader}
				/delay 6
				/call cCastProperSpellRank "Spirit of the Kraken"
				/delay 6
				/assist ${MainAssist}
			}
		}
	}

	/if (${Cast.Ready[Ancestral Grudge]} && !${Me.Song[Ancestral Grudge].ID}) {
		/call cCastProperSpellRank "Ancestral Grudge"
	}
	/if (${Me.Buff[Champion].Duration} < 60000) {
		/call cCastProperSpellRank "Champion"
	}
	/if (!${Target.Buff[Snake's Kiss].ID}) {
		/call cCastProperSpellRank "Blasphemous Tongue"
	}
	/if (!${Target.Buff[Baited Breath].ID}) {
		/call cCastProperSpellRank "Baited Breath"
	}
	
	|/if (${Cast.Ready[Frozen Fury]}) {
	|	/cast 9
	|}
	
/return


Sub SpamCleric
	
	/if (${Me.Buff[Amplify Healing].Duration} < 60000) {
		/call cCastProperSpellRank "Amplify Healing"
	}
	
/return


Sub SpamBard
	
	/if (${FindItem[Farseeker's Plate Chestguard of Harmony].TimerReady}==0) {
		/casting "Farseeker's Plate Chestguard of Harmony" item -maxtries|1
	}
	|/if (!${Me.Buff[Spirit of Vesagran].ID}) {
	|	/itemnotify 13 rightmouseup
	|}
	
	/if (!${Me.Song[One Man Band].ID}) {
		/declare BardArray[3] string local
		/varset BardArray[1] ClydeBard
		/varset BardArray[2] ClydeBardToo
		/varset BardArray[3] ClydeBrdA
		/if (${Me.Name.Equal[${BardArray[1]}]}) {
			/delay 1s
			/if (!${Me.Song[One Man Band].ID}) {
				/itemnotify 13 rightmouseup
			}
		} else /if (${Me.Name.Equal[${BardArray[2]}]}) {
			/delay 2s
			/if (!${Me.Song[One Man Band].ID}) {
				/itemnotify 13 rightmouseup
			}
		} else /if (${Me.Name.Equal[${BardArray[3]}]}) {
			/delay 3s
			/if (!${Me.Song[One Man Band].ID}) {
				/itemnotify 13 rightmouseup
			}
		}
	}
	
	/if (${Cast.Ready[Boastful Bellow]}) /casting "Boastful Bellow"
	
/return


Sub SpamPaladin

	/if (!${Me.Buff[Focus of Healing].ID}) {
		/casting "Focus of Healing" gem3 -maxtries|1
	}

	/if (!${Me.Song[Yaulp].ID}) {
		/call cCastProperSpellRank "Yaulp"
	}
	|/if (${Cast.Ready[Zealous Smite]}) {
	|	/squelch /noparse /casting "Zealous Smite" gem2 -maxtries|1
	|}
	
/return


Sub SpamDruid

	|/if (!${Me.Buff[Focus of Healing].ID}) {
	|	/casting "Focus of Healing" gem3 -maxtries|1
	|}
	
	|/if (!${Me.Buff[Wisest Healer].ID}) {
	|	/casting "Crystalline Idol of Healing Boon"
	|}	
	|
	|/if (${Me.Buff[Timeless: Pack Regeneration].Duration} < 60000) {
	|	/call cCastProperSpellRank "Timeless: Pack Regeneration"
	|}
	|
	|
	|/if (!${Target.Buff[Vine Grasp].ID}) {
	|	/itemnotify 13 rightmouseup
	|}
	
/return


Sub SpamRogue

	/if (${Me.AbilityReady[Backstab]}) /doability Backstab

	/if (!${FindItem[=Spirit of the Ninja Proc (Reward Item)].TimerReady} && !${Me.Buff[Spirit of the Ninja].ID}) {
		/itemnotify  "Spirit of the Ninja Proc (Reward Item)" rightmouseup
	}

	/if (!${FindItem[Nightshade, Blade of Entropy 2.5].TimerReady}) {
		/itemnotify  "Nightshade, Blade of Entropy 2.5" rightmouseup
	}
	
	/if (!${FindItem[Fatestealer].TimerReady}) {
		/itemnotify  "Fatestealer" rightmouseup
	}

	/if (!${Me.Song[Thief's eyes].ID}) {
		/disc Thief's eyes
	}
	
	/if (${Me.AltAbilityReady[Rogue's Fury]}) {
		/casting "Rogue's Fury"
	}
	
	
	/if (${FindItem[Whispering Tunic of Shadows].TimerReady}==0) {
		/casting "Whispering Tunic of Shadows" item -maxtries|1
	}
	
	/if (${Cast.Ready[Twisted Shank]}) /casting "Twisted Shank"
	
	
	
	|/disc Ancient: Chaos Strike
	
/return


Sub SpamRanger


	/if (!${Me.Buff[Focus of Healing].ID}) {
		/casting "Focus of Healing" gem3 -maxtries|1
	}
	
	/if (${FindItem[Bladewhisper Chain Vest of Journeys].TimerReady}==0) {
		/casting "Bladewhisper Chain Vest of Journeys" item -maxtries|1
	}

	|/call cCastProperSpellRank "Burning Tinder"
	
	|/if (${Cast.Ready[Ancient: North Wind]}) {
	|	/casting "Ancient: North Wind" gem2 -maxtries|1
	|}
	
	/itemnotify 13 rightmouseup
	
	
/return


Sub SpamNecromancer

	/if (${Me.Pet.Find[NO PET]}) {
		/casting ${Me.Inventory[mainhand].AugSlot[0]}
	}
	
	/cast 1
	
	
/return


Sub SpamMagician

	/if (${Me.Pet.Find[NO PET]}) {
		/casting ${Me.Inventory[mainhand].AugSlot[0]}
	}

	
	
/return


Sub SpamBeastlord

	/if (${Me.Pet.Find[NO PET]}) {
		/casting ${Me.Inventory[mainhand].AugSlot[0]}
	}

	
	
/return


Sub SpamWizard

	/if (${Me.Buff[Thick Ether Skin].Duration} < 80000) {
		/itemnotify 13 rightmouseup
	}

	
	
/return


Sub SpamMonk

		/itemnotify 13 rightmouseup

		/if (${FindItem[Crab Fu: The Way of the Quickening Fists].ID}) {
			/if (!${Me.Buff[Crab Fu: The Way of the Quickening Fists].ID}) {
				/itemnotify  "Crab Fu: The Way of the Quickening Fists" rightmouseup
			}
		}
	
	
/return
|----------