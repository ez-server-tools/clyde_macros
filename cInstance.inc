#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|====== 
|====== This is for managing instances of a zone you're currently in.
|====== 
|====== **IMPORTANT**
|====== Find below: FreeInstanceEnabledCharacterArray
|====== 	- And change the characters to your characters that you want this to work for
|====== 	- For me, I have 2 characters, so on the declare line it has [2]
|====== 	  If you have more/less than 2 characters, change this number
|====== 	  And make sure the numbering works on each characters add
|====== 
|====== It's meant to be used on characters that have the free instance item
|====== 
|====== I decided that creating an instance handler for remote instances
|====== was just kinda silly if I'm not getting paid, so zone in to the public zone first

#Event cInstanceCreate	    				"cI Cre"
#Event cInstanceCreateRemote	    		"cI Cre #1#"
#Event cInstanceEnter	    				"cI Ent"
#Event cInstanceEnterInstanceCommand		"cI Ent #1#"
#Event cInstanceEnterInstanceCommand		"cI EntME #1#"

|====== Note for add: enter the exact thing you'd do to enter that instance (minus /say)
|====== 
|====== Ex: /echo cI Add enter guild qekiibre qvic
#Event cInstanceAdd	    					"cI Add #1#"

|====== Note for remove: enter the instance number. You can check this in the ini, or by running the "cList" command
|====== --Special Note: This will not completely remove it from the ini, just clobber the value.
|======   so you'll need to clean that up manually in the ini (after deleting and running cList, you'll see NULL)
|====== Ex: /echo cI Rem 1
#Event cInstanceRemove	    				"cI Rem #1#"


#Event cInstanceList	    				"cI List"
#Event cOpen								"cI Open"
#Event cOpen								"cI Open #1#"


|====== This is blanket watching and will be working in the background if you manage instances manually too
#Event cInstanceCreated 		"#*# to view, enter #1# #2# #3# to zone#*#"
#Event cNoSuchInstance 		"No instance found with request"
#Event cCantZone 			"You cannot use these commands while in combat"

|==== Sub Event_cInstance
	Sub Event_cInstanceCreateZone(Line, cCommand)
		/call cCreateInstanceLogic ${cCommand}
	/return
	
	Sub Event_cInstanceCreate	
		/call cCreateInstanceLogic
	/return
		Sub Event_cInstanceCreateRemote(Line, cCommand)
		/call cCreateInstanceLogic ${cCommand}
	/return
|==== End Sub Event_cInstance

|==== Sub Event_cInstanceEnter
	Sub Event_cInstanceEnterInstanceCommand(Line, cCommand)
		/call cInstanceEnterLogic ${cCommand}
	/return
	
	Sub Event_cInstanceEnter	
		/call cInstanceEnterLogic
	/return
|==== End Sub Event_cInstance


Sub cCreateInstanceLogic(string RemoteZoneName)

	/if (${Me.CombatState.Equal[COMBAT]}) {
		/echo \a-rCan't create instances in combat!
		/return
	}

	/declare FreeInstanceEnabledCharacters string ${Ini[${cMacIni},cInstCharacters]}
	/declare ZoneName string local ${Zone.ShortName}
	
	/if (${RemoteZoneName.Length}) {
		/varset ZoneName ${RemoteZoneName}
	}	

	/if (${FreeInstanceEnabledCharacters.Find[${Me.Name}]}) {
		/say create guild instance ${ZoneName}
		/delay 5
		/say create raid instance ${ZoneName}
		/delay 5
	}
	
/return


Sub cInstanceEnterLogic(string cCommand)

	/if (${Me.CombatState.Equal[COMBAT]}) {
		/echo \a-rCan't enter instances in combat!
		/return
	}

	/declare TotalDeclaredInstances int local ${Get_NumOccurancesFromIniSection[${Ini[${cMacIni},cInstances]}]}
	/declare EnterText string local
	/declare IndexWhenStartLoopingIndexes int local 0
	
	/if (${TotalDeclaredInstances}==0) {
		/echo No instances yet!
		/return
	}
	
	/echo hi

	/if (!${Defined[CurrentInstanceIndex]}) /declare CurrentInstanceIndex int outer
	/varset CurrentInstanceIndex ${Ini[${cMacIni},cInstance,CurrentInstance]}
	
	/if (${cCommand.Length} && ${cCommand.NotEqual[me]}) {
		/varset CurrentInstanceIndex ${cCommand}
	} else {
		/varcalc CurrentInstanceIndex ${CurrentInstanceIndex}+1
	}
	
	/if (${CurrentInstanceIndex}>${TotalDeclaredInstances}) {
		/varset CurrentInstanceIndex 1
	}
	
	/varset EnterText ${Ini[${cMacIni},cInstances,${CurrentInstanceIndex}]}

	/if (${EnterText.Equal[NULL]}) {
		/varset IndexWhenStartLoopingIndexes ${CurrentInstanceIndex}
		/while (${EnterText.Equal[NULL]}) {
			/varcalc CurrentInstanceIndex ${CurrentInstanceIndex}+1
			
			/varset EnterText ${Ini[${cMacIni},cInstances,${CurrentInstanceIndex}]}
			
			/if (${IndexWhenStartLoopingIndexes}==${CurrentInstanceIndex}) {
				/echo No such instance
				/return
			}
		}
	}
	
	/echo \ayEntering: \aw${EnterText} \axIdx now: \a-w${CurrentInstanceIndex}
	/docommand /${AnnounceChannel} Entering Instance: ${EnterText}
	/ini "${cMacIni}" "cInstance" "CurrentInstance" "${CurrentInstanceIndex}"

	/if (${cCommand.Equal[me]}) {
		/docommand /say ${EnterText}
	} else {
		/docommand ${BroadCastToAllOthersSelf}say ${EnterText}
	}
	
	
/return

| word1: Raid/Guild, word2: Owner, word3: Zone
Sub Event_cInstanceCreated(Line, word1, word2, word3)
	/echo Happening
	/if (${word2.Equal[${Me.Name}]}) {
		/echo Zone: ${word3}
		
		/docommand /${AnnounceChannel}  ${Me.Name} Has Created ${word1} instance in ${word3}
		
		/declare NextInstanceNumber int local ${Get_NumOccurancesFromIniSection[${Ini[${cMacIni},cInstances]}]}
		/varcalc NextInstanceNumber ${NextInstanceNumber}+1	
		/ini "${cMacIni}" "cInstances" "${NextInstanceNumber}" "enter ${word1} ${Me.Name} ${word3}"
	}	
/return


Sub Event_cInstanceList
	
	/if (!${Defined[CurrentInstanceIndex]}) 	/declare CurrentInstanceIndex int outer
	
	/declare CurrInstanceIndicator string local
	/declare InstanceListToPrint string local
	/varset CurrentInstanceIndex ${Ini[${cMacIni},cInstance,CurrentInstance]}
	/varset InstanceListToPrint ${Ini[${cMacIni},cInstances]}

	/if (${InstanceListToPrint.Equal[NULL]}) {
		/echo No instances yet!
		/return
	} else {
		/declare InstanceNumber int local 1
		/for InstanceNumber 1 to ${Get_NumOccurancesFromIniSection[${Ini[${cMacIni},cInstances]}]} {
			/if (${CurrentInstanceIndex}==${InstanceNumber}) {
				/varset CurrInstanceIndicator <--\a-wCurrent
			} else {
				/varset CurrInstanceIndicator
			}
			/if (${CurrInstanceIndicator.Length}) {
				/echo Instance \a-w${InstanceNumber}\ax: \a-g${Ini[${cMacIni},cInstances,${InstanceNumber}]} \ax${CurrInstanceIndicator}
			} else {
				/echo Instance \at${InstanceNumber}\ax: ${Ini[${cMacIni},cInstances,${InstanceNumber}]}
			}
			
			/next InstanceNumber
		}	
	}

/return


Sub Event_cInstanceAdd(Line, word1)
	/declare NextInstanceNumber int local ${Get_NumOccurancesFromIniSection[${Ini[${cMacIni},cInstances]}]}
	/varcalc NextInstanceNumber ${NextInstanceNumber}+1
	
	/docommand /${AnnounceChannel} Adding ${word1} to INI at index ${NextInstanceNumber}
	
	/ini "${cMacIni}" "cInstances" "${NextInstanceNumber}" "${word1}"
/return


Sub Event_cInstanceRemove(Line, word1)
	/docommand /${AnnounceChannel} Removing instance from INI at index: ${word1}
	/ini "${cMacIni}" "cInstances" "${word1}" ""
/return


Sub Event_cNoSuchInstance
	/if (${Ini[${cMacIni},cInstance,CurrentInstance]}!=0) {
		/echo This instance isn't found, iterating current instance index
		/varcalc CurrentInstanceIndex ${CurrentInstanceIndex}+1
	}
/return


Sub Event_cCantZone
	/if (${Defined[CurrentInstanceIndex]}) {
		/docommand /${AnnounceChannel} I could not change instances, I could be left behind!
	}
/return


Sub Event_cOpen(Line, word1)
	/if (${word1.Length}) {
		/say set_instance_open ${word1}
	} else {
		/say set_instance_open ${Me.Instance}
	}	
/return


Sub Get_NumOccurancesFromIniSection(string SectionToCheck)
	/declare NumOccurances int local 0

	/if (!${SectionToCheck.Equal[NULL]}) {
		/varset NumOccurances ${SectionToCheck.Count[|]}
		/varcalc NumOccurances ${NumOccurances}-2
	}
	
/return ${NumOccurances}


|----------