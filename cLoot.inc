#warning



|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.


#include cMacros\cCommonUtils.inc


#Event cLoot_CL				"cLoot #1#"


#Event cTooFarAwayEvent_CL 		"#*#You are too far away to loot that corpse#*#"
#Event cWaitLootEvent_CL 		"#*#Someone is already looting that corpse#*#"
#Event cAcquireTargetEvent_CL 	"#*#You must first target a corpse to loot#*#"


|====  Sub Event_cLoot_CL
	Sub Event_cLoot_CL(Line, cLootFile)
		/call cLootLogic_CL ${cLootFile}
	/return
|==== /Sub Event_cLoot_CL


Sub cLootLogic_CL(cLootFile)	
	/if (!${Defined[cLootIni_CL]}) 			/declare cLootIni_CL 			string outer cMacros\cLootIni\cLoot_${cLootFile}.ini
	/if (!${Defined[allItems_CL]})      	/declare allItems_CL   			string outer ${Ini[${cLootIni_CL},LootAll]}
	/if (!${Defined[someItems_CL]})     	/declare someItems_CL   		string outer ${Ini[${cLootIni_CL},SomeItems]}
	/if (!${Defined[singleItems_CL]})   	/declare singleItems_CL 		string outer ${Ini[${cLootIni_CL},SingleItems]}
	/if (!${Defined[destroyItems_CL]})  	/declare destroyItems_CL		string outer ${Ini[${cMacIni},DestroyLoot]}
	/if (!${Defined[announceItems_CL]}) 	/declare announceItems_CL		string outer ${Ini[${cMacIni},AnnounceLoot]}
	
	
	/if (!${Defined[ConfirmYesNoDrop]})		/declare ConfirmYesNoDrop 		int outer ${Ini[${cMacIni},cLoot,ConfirmYesNoDrop]}
	/if (!${Defined[CurrentTargetId_CL]}) 	/declare CurrentTargetId_CL 	int outer
	/if (!${Defined[TooBigCorpseList_CL]}) 	/declare TooBigCorpseList_CL 	string outer
	
	/if (!${Defined[NumCorpses]}) 			/declare NumCorpses 			int local ${SpawnCount[npccorpse radius ${Ini[${cMacIni},cLoot,LootRadius]}]}
	/if (!${Defined[FarCorspeFlag]}) 		/declare FarCorspeFlag 			int local
	

	
	/echo \a-tcLoot: \a-gSTARTED
	/hidecorpse looted
	/say #corpsefix
	/varset LootActiveFlag 1
	/varset TooBigCorpseList_CL

	/while (${SpawnCount[npccorpse radius ${Ini[${cMacIni},cLoot,LootRadius]}]}) {
		/while (${Cursor.ID}) {
			/doevents
			/autoi
			/delay 1
		}
	
		/if (!${cNumOpenSlots[PlaceHolder]}) {
			/echo \arBags Full!
			/return
		}
	
		/call AcquireTarget_CL
		
		
		/if (!${SpawnCount[npccorpse id ${CurrentTargetId_CL}]} || !${Target.ID}) {
			/continue
		}
		
		/face fast
		
		/if (${Target.Distance} > 10) {
			/if (${Target.Distance}>60) {
				/varset FarCorspeFlag 1
			}
			/moveto id ${CurrentTargetId_CL}
			/delay 1
			/while (${MoveUtils.Command.Equal[MOVETO]}) {
				/doevents
				/delay 1
			}
			/if (${FarCorspeFlag}) {
				/say #corpsefix
			}
		}
		
		/call LootRoutine_CL
	}
	
	/varset LootActiveFlag 0
	/squelch /stick off
	/moveto yloc ${Math.Calc[${Int[${Me.Loc}]}+30]}
	/echo \a-tcLoot: \a-rENDED
/return


| ----------------------------------------------------------------------------------------------------------
Sub LootRoutine_CL
	/if (!${Defined[LootList]})	 			/declare LootList    			string local
	/if (!${Defined[RetryLootCount]}) 		/declare RetryLootCount			int local
	
	
	/if (!${Defined[CorpseItemIndex_CL]})	/declare CorpseItemIndex_CL 	int outer
	/if (!${Defined[NumItemsOnCorpse]})		/declare NumItemsOnCorpse 		int local 
	/if (!${Defined[ItemName_CL]}) 			/declare ItemName_CL 			string outer
	
	/loot
	/varset RetryLootCount 1
	/while (!${Corpse.Open} || !${Window[LootWnd].Open}) {
		/doevents
		/if (${RetryLootCount} > 60) {
			/delay 1s
			/say #corpsefix
		}	
		/if (!${SpawnCount[npccorpse id ${CurrentTargetId_CL}]} || ${RetryLootCount} > 120) {
			/return
		}
		/loot
		/delay 1
		/varcalc RetryLootCount ${RetryLootCount}+1
	}

	/varset CorpseItemIndex_CL 1
	/while (${Corpse.Item[${CorpseItemIndex_CL}].ID}) {
		/if (${CorpseItemIndex_CL} > 30 && !${TooBigCorpseList_CL.Find[${CurrentTargetId_CL}|]}) {
			/varset TooBigCorpseList_CL ${TooBigCorpseList_CL}${CurrentTargetId_CL}|
			/if (${Ini[${cMacIni},cLoot,MasterAnnouncer].Equal[${Me.Name}]} || ${Ini[${cMacIni},cLoot,MassAnnounce]]}) {
				/docommand /${AnnounceChannel} Too big of a corpse on ID - ${CurrentTargetId_CL} Check for more loot
			}
			/break
		}
		/doevents
		/varset LootList ${LootList}${Corpse.Item[${CorpseItemIndex_CL}]}|
		/varcalc CorpseItemIndex_CL ${CorpseItemIndex_CL}+1		
	}	
	
	/varset CorpseItemIndex_CL 1
	/while (${LootList.Arg[${CorpseItemIndex_CL},|].Length}) {	
		/doevents
		/if (!${SpawnCount[npccorpse id ${CurrentTargetId_CL}]}) {
			/return
		}
	
		/varset ItemName_CL ${LootList.Arg[${CorpseItemIndex_CL},|]}
		
		
		/if (${announceItems_CL.Find[${ItemName_CL}]}) {
		
			/echo \ar!!!!! \ap${ItemName_CL} \axfound on ID - \aw${CurrentTargetId_CL} \ar!!!!!
			/if (${Ini[${cMacIni},cLoot,MasterAnnouncer].Equal[${Me.Name}]} || ${Ini[${cMacIni},cLoot,MassAnnounce]]}) {
				/docommand /${AnnounceChannel} ${Corpse.Item[${CorpseItemIndex_CL}].ItemLink[CLICKABLE]} found on ID - ${CurrentTargetId_CL}
			}
			
			
			} else /if (${someItems_CL.Find[|${ItemName_CL}|]}) {
			
				/if (!${Defined[CountOnSelf]}) 		/declare CountOnSelf 		int local ${FindItemCount[=${ItemName_CL}]}
				/if (!${Defined[CountInBank]}) 		/declare CountInBank 		int local ${FindItemBankCount[=${ItemName_CL}]}
				/if (!${Defined[HowManyToLootNum]}) /declare HowManyToLootNum	int local ${Ini[${cLootIni_CL},SomeItems,${ItemName_CL}]}
				/if (!${Defined[TotalAmountOwned]}) /declare TotalAmountOwned	int local ${Int[${Math.Calc[${CountOnSelf}+${CountInBank}]}]}
				
				
				/if (${TotalAmountOwned} < ${HowManyToLootNum}) {
					/varcalc TotalAmountOwned ${TotalAmountOwned}+1
					/call LootSomeRoutine_CL ${TotalAmountOwned} ${HowManyToLootNum}
				} else {
					/echo \aw${ItemName_CL} \axon \a-w${CurrentTargetId_CL} \ax- slot \a-g${CorpseItemIndex_CL}
				}
				
			} else /if (${singleItems_CL.Find[|${ItemName_CL}|]}) {
			
				/call LootSingleRoutine_CL
				
			} else /if (${allItems_CL.Find[|${ItemName_CL}|]}) {
			
				/call LootAllRoutine_CL
				
			} else /if (${destroyItems_CL.Find[|${ItemName_CL}|]} && ${Ini[${cMacIni},cLoot,DestroyEnabled]}) {
			
				/call DestroyRoutine_CL
				
			} else {
			
				/echo \aw${ItemName_CL} \axon \a-w${CurrentTargetId_CL} \ax- slot \a-g${CorpseItemIndex_CL}
				
			}
	
	
		/varcalc CorpseItemIndex_CL ${CorpseItemIndex_CL}+1
	}
	
	/while (${Corpse.Open} || ${Window[LootWnd].Open}) {
		/doevents
		/notify LootWnd DoneButton leftmouseup
		/delay 1
	}
	/varset LootList
	
/return

| -------------------------------------||-- LootRoutines --||-----------------------------------------------
Sub LootAllRoutine_CL
	/echo [lootALL] \agLooting \ap${ItemName_CL} \axon \a-w${CurrentTargetId_CL} \ax- slot \a-g${CorpseItemIndex_CL}
	
	/call LootItem_CL
/return
| ----------------------------------------------------------------------------------------------------------
Sub LootSomeRoutine_CL(int AlreadyHaveNum,int HowManyToLootNum)
	/echo [lootSome] \agLooting \ap${ItemName_CL} \ax- \aw${AlreadyHaveNum} \ax/ \aw${HowManyToLootNum} \axon \a-w${CurrentTargetId_CL} \ax- slot \a-g${CorpseItemIndex_CL}
	
	/call LootItem_CL
/return
| ----------------------------------------------------------------------------------------------------------
Sub LootSingleRoutine_CL
	/if (${FindItem[=${ItemName_CL}].ID} || ${FindItemBank[=${ItemName_CL}].ID}) {
		/echo \aw${ItemName_CL} \axon \a-w${CurrentTargetId_CL} \ax- slot \a-g${CorpseItemIndex_CL}
		/return
	}
	/echo [lootSingle] \agLooting \ap${ItemName_CL} \axon \a-w${CurrentTargetId_CL} \ax- slot \a-g${CorpseItemIndex_CL}
	/call LootItem_CL
/return
| ----------------------------------------------------------------------------------------------------------
Sub DestroyRoutine_CL
	/echo [\a-rDESTROY\ax] \ap${ItemName_CL} \axon \a-w${CurrentTargetId_CL} \ax- slot \a-g${CorpseItemIndex_CL}
	/delay 1
	/autoi
	/delay 1
	/autoi
	/while (${Corpse.Item[${CorpseItemIndex_CL}].ID}) {
	
		/doevents
		/ctrl /itemnotify loot${CorpseItemIndex_CL} leftmouseup
		/if (${Window[ConfirmationDialogBox].Open} && ${ConfirmYesNoDrop}) {
			/squelch /yes
		} else {
			/squelch /no
		}
		/delay 1
	}
	/while (!${Cursor.ID}) {
		/doevents
		/delay 1
	}
	/destroy
	/while (${Cursor.ID}) {
		/doevents
		/delay 1
	}
/return
| ------------------------------------||-- /LootRoutines --||-----------------------------------------------





| -----------------------------------------||-- UTILS --||--------------------------------------------------
Sub AcquireTarget_CL(int TargetID)
	/if (${TargetID}) {
		/tar id ${TargetID}
	} else {
		/varset TargetID ${NearestSpawn[npccorpse radius ${Ini[${cMacIni},cLoot,LootRadius]}].ID}
		/tar id ${TargetID}
	}
	
	/while (!${Target.ID} && ${SpawnCount[npccorpse id ${TargetID}]}) {	
		/doevents
		/tar id ${NearestSpawn[npccorpse radius ${Ini[${cMacIni},cLoot,LootRadius]}].ID}
		/delay 1
	}
	/varset CurrentTargetId_CL ${Target.ID}
/return
| ----------------------------------------------------------------------------------------------------------
Sub HaveLoreItemCheck_CL(string DummyString)
	/if (${FindItem[=${ItemName_CL}].ID} || ${FindItemBank[=${ItemName_CL}].ID}) {
		/return 1
	}
/return 0
| ----------------------------------------------------------------------------------------------------------
Sub LootItem_CL
	/if (${Corpse.Item[${CorpseItemIndex_CL}].Lore} && ${HaveLoreItemCheck_CL[${ItemName_CL}]}) {
		/echo \amHave Lore: \aw${ItemName_CL} \axon \a-w${CurrentTargetId_CL}
		/if (${Ini[${cMacIni},cLoot,MasterAnnouncer].Equal[${Me.Name}]} || ${Ini[${cMacIni},cLoot,MassAnnounce]]}) {
			/docommand /docommand /${AnnounceChannel} CAN'T LOOT LORE ITEM: ${Corpse.Item[${CorpseItemIndex_CL}].ItemLink[CLICKABLE]} found on ID - ${CurrentTargetId_CL}
		}
		/return
	}
	
	/while (${Corpse.Item[${CorpseItemIndex_CL}].ID}) {
		/doevents
		/ctrl /itemnotify loot${CorpseItemIndex_CL} rightmouseup
		/if (${Window[ConfirmationDialogBox].Open} && ${ConfirmYesNoDrop}) {
			/squelch /yes
		} else {
			/squelch /no
		}
		/delay 1
		/ctrl /itemnotify loot${CorpseItemIndex_CL} rightmouseup
	}
/return
| -----------------------------------------||-- /UTILS --||-------------------------------------------------


| -----------------------------------------||-- EVENTS --||--------------------------------------------------
Sub Event_cTooFarAwayEvent_CL
	/if (${LootActiveFlag}) {
		/moveto id ${CurrentTargetId_CL}
		/doevents
		/while (${MoveUtils.Command.Equal[MOVETO]}) {
			/doevents
			/delay 1
		}
		/say #corpsefix
	}
/return
| ----------------------------------------------------------------------------------------------------------
Sub Event_cWaitLootEvent_CL
	/if (${LootActiveFlag}) {
		/delay 1s
	}
/return
| ----------------------------------------------------------------------------------------------------------
Sub Event_cAcquireTargetEvent_CL
	/if (${LootActiveFlag}) {
		/call AcquireTarget_CL
	}
/return
| -----------------------------------------||-- /EVENTS --||-------------------------------------------------