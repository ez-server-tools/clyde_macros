#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.


#Event cBuff		"cBuff"


|==== Sub Event_cBuff
	Sub Event_cBuff
		/call cBuffLogic
	/return
|==== End Sub Event_cBuff



Sub cBuffLogic
	
	/stand
	/delay 1m ${Me.Standing}
	

	/if (${Me.Class.Name.Equal[Warrior]}) {
		/call BuffWarrior
	}
	/if (${Me.Class.Name.Equal[Berserker]}) {
		/call BuffBerserker
	}
	/if (${Me.Class.Name.Equal[Shaman]}) {
		/call BuffShaman
	}
	/if (${Me.Class.Name.Equal[Cleric]}) {
		/call BuffCleric
	}
	/if (${Me.Class.Name.Equal[Bard]}) {
		/call BuffBard
	}
	/if (${Me.Class.Name.Equal[Paladin]}) {
		/call BuffPaladin
	}
	/if (${Me.Class.Name.Equal[Druid]}) {
		/call BuffDruid
	}
	/if (${Me.Class.Name.Equal[Rogue]}) {
		/call BuffRogue
	}
	/if (${Me.Class.Name.Equal[Ranger]}) {
		/call BuffRanger
	}
	/if (${Me.Class.Name.Equal[Necromancer]}) {
		/call BuffNecromancer
	}
/return

|-------------


Sub BuffWarrior
	|/docommand ${BroadcastEcho} Buff Warrior(${Me.Name}) Start	
	/if (${Me.CombatAbilityReady[Myrmidon's Aura]}) {
		/disc Myrmidon's Aura
	}
	|/delay 5
	/docommand ${BroadcastEcho} Buff Warrior(${Me.Name}) Done
/return


Sub BuffBerserker	
	|/docommand ${BroadcastEcho} Buff Berserker (${Me.Name}) Start
	|/docommand ${BroadcastEcho} Buff Berserker (${Me.Name}) Done	
/return


Sub BuffShaman
	|/docommand ${BroadcastEcho} Buff Shaman (${Me.Name}) Start	
	
	/call cCastProperSpellRank "${Me.Inventory[mainhand].AugSlot[0]},MGB,true"
	/call cCastProperSpellRank "Talisman of Sense,,true"
	/call cCastProperSpellRank "Talisman of Might,,true"
	/call cCastProperSpellRank "Form of Ancient Spirits,,true"
	
	|/delay 1m ${Cast.Ready}
	|/casting "Talisman of Sense"
	|/delay 1m ${Cast.Ready}
	|/casting "Talisman of Might"
	
	
	|/call cCastProperBuffRank "Runic Blessing of the Tribunal II"	
	|/casting "Smallest Terror"
	|/casting "Talisman of Wunshi"
	

	/docommand ${BroadcastEcho} Buff Shaman (${Me.Name}) Done	
/return


Sub BuffCleric
	|/docommand ${BroadcastEcho} Buff Cleric (${Me.Name}) Start
	
	/call cCastProperSpellRank "Armor of the Crab,MGB,true"
	
	/docommand ${BroadcastEcho} Buff Cleric (${Me.Name}) Done	
/return


Sub BuffBard	
	|/docommand ${BroadcastEcho} Buff Bard (${Me.Name}) Start		
	
	/if (!${Me.Song[Sionachie's Crescendo].ID}) {
		/twist stop
		/delay 15
		/casting "Sionachie's Crescendo"
		/delay 15
	}
	/if (!${Me.Song[Aura of Insight].ID}) {
		/twist stop
		/twist 2
		/delay 5s
		/twist stop
	}
	
	/docommand ${BroadcastEcho} Buff Bard (${Me.Name}) Done	
/return


Sub BuffPaladin
	|/docommand ${BroadcastEcho} Buff Paladin (${Me.Name}) Start	
	
	/call cCastProperSpellRank "Kaldar's Helping Hand,,true"
	/call cCastProperSpellRank "Gift of the Avenger,,true"
	/call cCastProperSpellRank "Silent Piety,,true"
		
	/docommand ${BroadcastEcho} Buff Paladin (${Me.Name}) Done	
/return


Sub BuffDruid
	|/docommand ${BroadcastEcho} Buff Druid (${Me.Name}) Start
	
	/call cCastProperSpellRank "Blessing of Ancient Oak,MGB,true"
	/call cCastProperSpellRank "Form of the Werewolf,,true"
	
	|/if (!${Me.Song[Aura of the Grove].ID}) {
	|	/casting "Aura of the Grove"
	|	/delay 4s
	|}
	
	/docommand ${BroadcastEcho} Buff Druid (${Me.Name}) Done
/return


Sub BuffRogue
	|/docommand ${BroadcastEcho} Buff Rogue (${Me.Name}) Start	
	
	|/docommand ${BroadcastEcho} Buff Rogue (${Me.Name}) Done	
/return


Sub BuffRanger
	|/docommand ${BroadcastEcho} Buff Ranger (${Me.Name}) Start
	
	/call cCastProperSpellRank "Call of Lightning,,true"
	/call cCastProperSpellRank "Brushcoat,,true"
	/call cCastProperSpellRank "Secrets' Secret Ranger Secrets,,true"
	/call cCastProperSpellRank "Howl of the Huntmaster,MGB,true"
	
	/docommand ${BroadcastEcho} Buff Ranger (${Me.Name}) Done	
/return


Sub BuffNecromancer


	
	
/return

|----------