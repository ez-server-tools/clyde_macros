#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|======

#Event cSoAMaker	"cSoA"

|==== Sub Event_cEoNMaker
	Sub Event_cSoAMaker
		/call cSoAMakerLogic
	/return
|==== End Sub Event_cSoAMaker

|Have an empty Magic Box in slot 10
Sub cSoAMakerLogic

	/if (!${Cursor.Name.Find[Shield of the Ages]}) {
		/echo Shield not on Cursor, please put it on cursor
		/return
	}

	/declare EssenceArray[61] string
	/varset EssenceArray[1] Essence of Qvic
	/varset EssenceArray[2] Essence of Qvic
	/varset EssenceArray[3] Essence of Qvic
	/varset EssenceArray[4] Essence of Qvic
	/varset EssenceArray[5] Essence of Qvic
	/varset EssenceArray[6] Essence of Cazic Thule
	/varset EssenceArray[7] Essence of Cazic Thule
	/varset EssenceArray[8] Essence of Cazic Thule
	/varset EssenceArray[9] Essence of Cazic Thule
	/varset EssenceArray[10] Essence of Cazic Thule
	/varset EssenceArray[11] Essence of Dragons Minor
	/varset EssenceArray[12] Essence of Dragons Minor
	/varset EssenceArray[13] Essence of Dragons Minor
	/varset EssenceArray[14] Essence of Dragons Minor
	/varset EssenceArray[15] Essence of Dragons Minor
	/varset EssenceArray[16] Essence of Dragons Major
	/varset EssenceArray[17] Essence of Dragons Major
	/varset EssenceArray[18] Essence of Dragons Major
	/varset EssenceArray[19] Essence of Dragons Major
	/varset EssenceArray[20] Essence of Dragons Major
	/varset EssenceArray[21] Essence of Gods Minor
	/varset EssenceArray[22] Essence of Gods Minor
	/varset EssenceArray[23] Essence of Gods Minor
	/varset EssenceArray[24] Essence of Gods Minor
	/varset EssenceArray[25] Essence of Gods Minor
	/varset EssenceArray[26] Essence of Gods Major
	/varset EssenceArray[27] Essence of Gods Major
	/varset EssenceArray[28] Essence of Gods Major
	/varset EssenceArray[29] Essence of Gods Major
	/varset EssenceArray[30] Essence of Gods Major
	/varset EssenceArray[31] Essence of The Abyss
	/varset EssenceArray[32] Essence of The Abyss
	/varset EssenceArray[33] Essence of The Abyss
	/varset EssenceArray[34] Essence of The Abyss
	/varset EssenceArray[35] Essence of The Abyss
	/varset EssenceArray[36] Essence of Anguish
	/varset EssenceArray[37] Essence of Anguish
	/varset EssenceArray[38] Essence of Anguish
	/varset EssenceArray[39] Essence of Anguish
	/varset EssenceArray[40] Essence of Anguish
	/varset EssenceArray[41] Essence of Loping Plains
	/varset EssenceArray[42] Essence of Loping Plains
	/varset EssenceArray[43] Essence of Loping Plains
	/varset EssenceArray[44] Essence of Loping Plains
	/varset EssenceArray[45] Essence of Loping Plains
	/varset EssenceArray[46] Essence of Temple Veeshan
	/varset EssenceArray[47] Essence of Temple Veeshan
	/varset EssenceArray[48] Essence of Temple Veeshan
	/varset EssenceArray[49] Essence of Temple Veeshan
	/varset EssenceArray[50] Essence of Temple Veeshan
	/varset EssenceArray[51] Essence of Old Commons
	/varset EssenceArray[52] Essence of Old Commons
	/varset EssenceArray[53] Essence of Old Commons
	/varset EssenceArray[54] Essence of Old Commons
	/varset EssenceArray[55] Essence of Old Commons
	/varset EssenceArray[56] Essence of Sunderock
	/varset EssenceArray[57] Essence of Sunderock
	/varset EssenceArray[58] Essence of Sunderock
	/varset EssenceArray[59] Essence of Sunderock
	/varset EssenceArray[60] Essence of Sunderock
	/varset EssenceArray[61] Essence of Sunderock

	
	/declare OreArray[61] string
	/varset OreArray[1] Magical Iron Ore
	/varset OreArray[2] Magical Iron Ore
	/varset OreArray[3] Magical Iron Ore
	/varset OreArray[4] Magical Iron Ore
	/varset OreArray[5] Magical Iron Ore
	/varset OreArray[6] Magical Steel Ore
	/varset OreArray[7] Magical Steel Ore
	/varset OreArray[8] Magical Steel Ore
	/varset OreArray[9] Magical Steel Ore
	/varset OreArray[10] Magical Steel Ore
	/varset OreArray[11] Magical Silver Bar
	/varset OreArray[12] Magical Silver Bar
	/varset OreArray[13] Magical Silver Bar
	/varset OreArray[14] Magical Silver Bar
	/varset OreArray[15] Magical Silver Bar
	/varset OreArray[16] Magical Gold Bar
	/varset OreArray[17] Magical Gold Bar
	/varset OreArray[18] Magical Gold Bar
	/varset OreArray[19] Magical Gold Bar
	/varset OreArray[20] Magical Gold Bar
	/varset OreArray[21] Magical Platinum Bar
	/varset OreArray[22] Magical Platinum Bar
	/varset OreArray[23] Magical Platinum Bar
	/varset OreArray[24] Magical Platinum Bar
	/varset OreArray[25] Magical Platinum Bar
	/varset OreArray[26] Magical Blue Diamond
	/varset OreArray[27] Magical Blue Diamond
	/varset OreArray[28] Magical Blue Diamond
	/varset OreArray[29] Magical Blue Diamond
	/varset OreArray[30] Magical Blue Diamond
	/varset OreArray[31] Magical Blue Diamond
	/varset OreArray[32] Magical Blue Diamond
	/varset OreArray[33] Magical Blue Diamond
	/varset OreArray[34] Magical Blue Diamond
	/varset OreArray[35] Magical Blue Diamond
	/varset OreArray[36] Magical Blue Diamond
	/varset OreArray[37] Magical Blue Diamond
	/varset OreArray[38] Magical Blue Diamond
	/varset OreArray[39] Magical Blue Diamond
	/varset OreArray[40] Magical Blue Diamond
	/varset OreArray[41] Magical Blue Diamond
	/varset OreArray[42] Magical Blue Diamond
	/varset OreArray[43] Magical Blue Diamond
	/varset OreArray[44] Magical Blue Diamond
	/varset OreArray[45] Magical Blue Diamond
	/varset OreArray[46] Magical Blue Diamond
	/varset OreArray[47] Magical Blue Diamond
	/varset OreArray[48] Magical Blue Diamond
	/varset OreArray[49] Magical Blue Diamond
	/varset OreArray[50] Magical Blue Diamond
	/varset OreArray[51] Magical Blue Diamond
	/varset OreArray[52] Magical Blue Diamond
	/varset OreArray[53] Magical Blue Diamond
	/varset OreArray[54] Magical Blue Diamond
	/varset OreArray[55] Magical Blue Diamond
	/varset OreArray[56] Magical Blue Diamond
	/varset OreArray[57] Magical Blue Diamond
	/varset OreArray[58] Magical Blue Diamond
	/varset OreArray[59] Magical Blue Diamond
	/varset OreArray[60] Magical Blue Diamond
	/varset OreArray[61] Magical Blue Diamond
	
	/declare ShieldNameArray[61] string
	/varset ShieldNameArray[1] Shield of the Ages Template
	/varset ShieldNameArray[2] Shield of the Ages I
	/varset ShieldNameArray[3] Shield of the Ages II
	/varset ShieldNameArray[4] Shield of the Ages III
	/varset ShieldNameArray[5] Shield of the Ages IV
	/varset ShieldNameArray[6] Shield of the Ages V
	/varset ShieldNameArray[7] Shield of the Ages VI
	/varset ShieldNameArray[8] Shield of the Ages VII
	/varset ShieldNameArray[9] Shield of the Ages VIII
	/varset ShieldNameArray[10] Shield of the Ages IX
	/varset ShieldNameArray[11] Shield of the Ages X
	/varset ShieldNameArray[12] Shield of the Ages XI
	/varset ShieldNameArray[13] Shield of the Ages XII
	/varset ShieldNameArray[14] Shield of the Ages XIII
	/varset ShieldNameArray[15] Shield of the Ages XIV
	/varset ShieldNameArray[16] Shield of the Ages XV
	/varset ShieldNameArray[17] Shield of the Ages XVI
	/varset ShieldNameArray[18] Shield of the Ages XVII
	/varset ShieldNameArray[19] Shield of the Ages XVIII
	/varset ShieldNameArray[20] Shield of the Ages XIX
	/varset ShieldNameArray[21] Shield of the Ages XX
	/varset ShieldNameArray[22] Shield of the Ages XXI
	/varset ShieldNameArray[23] Shield of the Ages XXII
	/varset ShieldNameArray[24] Shield of the Ages XXIII
	/varset ShieldNameArray[25] Shield of the Ages XXIV
	/varset ShieldNameArray[26] Shield of the Ages XXV
	/varset ShieldNameArray[27] Shield of the Ages XXVI
	/varset ShieldNameArray[28] Shield of the Ages XXVII
	/varset ShieldNameArray[29] Shield of the Ages XXVIII
	/varset ShieldNameArray[30] Shield of the Ages XXIX
	/varset ShieldNameArray[31] Shield of the Ages XXX
	/varset ShieldNameArray[32] Shield of the Ages XXXI
	/varset ShieldNameArray[33] Shield of the Ages XXXII
	/varset ShieldNameArray[34] Shield of the Ages XXXIII
	/varset ShieldNameArray[35] Shield of the Ages XXXIV
	/varset ShieldNameArray[36] Shield of the Ages XXXV
	/varset ShieldNameArray[37] Shield of the Ages XXXVI
	/varset ShieldNameArray[38] Shield of the Ages XXXVII
	/varset ShieldNameArray[39] Shield of the Ages XXXVIII
	/varset ShieldNameArray[40] Shield of the Ages XXXIX
	/varset ShieldNameArray[41] Shield of the Ages XL
	/varset ShieldNameArray[42] Shield of the Ages XLI
	/varset ShieldNameArray[43] Shield of the Ages XLII
	/varset ShieldNameArray[44] Shield of the Ages XLIII
	/varset ShieldNameArray[45] Shield of the Ages XLIV
	/varset ShieldNameArray[46] Shield of the Ages XLV
	/varset ShieldNameArray[47] Shield of the Ages XLVI
	/varset ShieldNameArray[48] Shield of the Ages XLVII
	/varset ShieldNameArray[49] Shield of the Ages XLVIII
	/varset ShieldNameArray[50] Shield of the Ages XLIX
	/varset ShieldNameArray[51] Shield of the Ages L
	/varset ShieldNameArray[52] Shield of the Ages LI
	/varset ShieldNameArray[53] Shield of the Ages LII
	/varset ShieldNameArray[54] Shield of the Ages LIII
	/varset ShieldNameArray[55] Shield of the Ages LIV
	/varset ShieldNameArray[56] Shield of the Ages LV
	/varset ShieldNameArray[57] Shield of the Ages LVI
	/varset ShieldNameArray[58] Shield of the Ages LVII
	/varset ShieldNameArray[59] Shield of the Ages LVIII
	/varset ShieldNameArray[60] Shield of the Ages LIX
	/varset ShieldNameArray[61] Shield of the Ages LX
	
	
	
	/if (!${Defined[SLS]}) /declare SLS string outer Superior Lightstone
	/if (!${Defined[EoN]}) /declare EoN string outer Essence of Norrath
	/declare EoNRanks string local |1|6|11|16|21|26|31|36|41|46|51|56|

	|This is the slot of the Magic Box I'll be using. Use 10 since I'm using Auto Inventory
	/if (!${Defined[MagicBoxSlot]}) /declare MagicBoxSlot string outer pack10
	/declare ItemIndex int local
	/declare StartIndex int local 1
	/declare CombineBoxSlot int local 1
	/declare EssenceName string local
	/declare OreName string local

	/keypress OPEN_INV_BAGS
	|Delay 1minute until that condition is met
	/delay 1m ${Window[pack8].Open}
	
	/while (${Cursor.Name.NotEqual[${ShieldNameArray[${StartIndex}]}]}) {
			/varcalc StartIndex ${StartIndex}+1
	}

	/for ItemIndex ${StartIndex} to 61
		/echo run: ${Cursor.Name} ${ItemIndex}
		/if (${ItemIndex}>60) {
			/return
		}
	
		/varset EssenceName ${EssenceArray[${ItemIndex}]}
		/varset OreName ${OreArray[${ItemIndex}]}

		
		/if (!${FindItem[=${EssenceName}].ID}) {
			/echo You don't have: ${EssenceName}
			/goto :Done
		}
		/if (!${FindItem[=${OreName}].ID}) {
			/echo You don't have: ${OreName}
			/goto :Done
		}
		/if (!${FindItem[=${SLS}].ID}) {
			/echo You don't have: ${SLS}
			/goto :Done
		}
		/if (${EoNRanks.Find[|${ItemIndex}|]}) {
			/if (!${FindItem[=${EoN}].ID}) {
				/echo You don't have: ${EoN}
				/goto :Done
			}
		}
		
		/if (${ItemIndex} >30) {
			/if (${FindItemCount[=${OreName}]} < 2) {
				/echo You don't have 2 ${OreName}
				/goto :Done
			}
		}
		/if (${ItemIndex} >35) {
			/if (${FindItemCount[=${OreName}]} < 3) {
				/echo You don't have 3 ${OreName}
				/goto :Done
			}
		}
		/if (${ItemIndex} >40) {
			/if (${FindItemCount[=${OreName}]} < 4) {
				/echo You don't have 4 ${OreName}
				/goto :Done
			}
			/if (${FindItemCount[=${EssenceName}]} < 2) {
				/echo You don't have 2 ${EssenceName}
				/goto :Done
			}			
			/if (${FindItem[=${SLS}].Stack} < 2) {
				/echo You don't have 2 SLS
				/goto :Done
			}
		}
		
		
		/itemnotify in ${MagicBoxSlot} 1 leftmouseup
		/delay 1m !${Cursor.ID}
		/echo Ess: Shield Num: ${ItemIndex}, Ess: ${EssenceArray[${ItemIndex}]}, Ore: ${OreName}
		/call PutItemInBag_cSoA "${EssenceArray[${ItemIndex}]}" 2
		/delay 1m ${Cursor.ID} != ${FindItem[=${EssenceArray[${ItemIndex}]}].ID}
		/call PutItemInBag_cSoA "${OreArray[${ItemIndex}]}" 3
		/delay 1m ${Cursor.ID} != ${FindItem[=${OreArray[${ItemIndex}]}].ID}
		/call PutItemInBag_cSoA "${SLS}" 4
		/delay 1m ${Cursor.ID} != ${FindItem[=${SLS}].ID}
		
		/if (${EoNRanks.Find[|${ItemIndex}|]}) {
			/call PutItemInBag_cSoA "${EoN}" 5
			/delay 1m ${Cursor.ID} != ${FindItem[=${EoN}].ID}
		}
		
		/if (${ItemIndex} >30) {
			/call PutItemInBag_cSoA "${OreArray[${ItemIndex}]}" 6
			/delay 1m ${Cursor.ID} != ${FindItem[=${OreArray[${ItemIndex}]}].ID}
		}
		/if (${ItemIndex} >35) {
			/call PutItemInBag_cSoA "${OreArray[${ItemIndex}]}" 7
			/delay 1m ${Cursor.ID} != ${FindItem[=${OreArray[${ItemIndex}]}].ID}
		}
		/if (${ItemIndex} >40) {
			/call PutItemInBag_cSoA "${EssenceArray[${ItemIndex}]}" 8
			/delay 1m ${Cursor.ID} != ${FindItem[=${EssenceArray[${ItemIndex}]}].ID}
			/call PutItemInBag_cSoA "${OreArray[${ItemIndex}]}" 9
			/delay 1m ${Cursor.ID} != ${FindItem[=${OreArray[${ItemIndex}]}].ID}
			/call PutItemInBag_cSoA "${SLS}" 10
			/delay 1m ${Cursor.ID} != ${FindItem[=${SLS}].ID}
		}

		
		/combine ${MagicBoxSlot}
		/delay 1m ${Cursor.ID}
		

		/next ItemIndex
		:Done
		/echo Done!
/return


Sub PutItemInBag_cSoA(string ItemName, int BagSlot)	
	/if (!${Defined[BagNum]}) /declare BagNum int local
	/varcalc BagNum ${FindItem[=${ItemName}].ItemSlot}
	/varcalc BagNum ${BagNum}-22
	/if (!${Defined[ItemBagSlot]}) /declare ItemBagSlot int local
	/varcalc ItemBagSlot ${FindItem[=${ItemName}].ItemSlot2}
	/varcalc ItemBagSlot ${ItemBagSlot}+1
	
	
	/ctrlkey /itemnotify in pack${BagNum} ${ItemBagSlot} leftmouseup
	/delay 1m ${Cursor.ID} == ${FindItem[=${ItemName}].ID}
	/itemnotify in ${MagicBoxSlot} ${BagSlot} leftmouseup
	/delay 1m !${Cursor.ID}
/return