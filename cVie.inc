#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.


#Event cTowerOfVie	"cVie"


#Event ClericReadyToCast 		"cVie - #1# is Ready to Cast #2# - cVie"


|==== Sub Event_cTowerOfVie
	Sub Event_cTowerOfVie
		/call cTowerOfVieLogic
	/return
|==== End Sub Event_cTowerOfVie

Sub cTowerOfVieLogic
	| ADD YOUR CLERICS TO THE ARRAY, on the declare ClericArray line, make sure you make the array big enough for your amount of clerics
	/declare ClericString string local ${Ini[${cMacIni},Clerics]}
	/declare NumClerics int local ${ClericString.Count[|]}	
	
	/declare RankArray[3] 	string 	local
	/declare ClericIndex 	int 	local
	/declare VieIndex 		int 	local
	/declare HasCasted 		bool 	local false
	
	/varset RankArray[1] Tower of Vie
	/varset RankArray[2] Tower of Vie II
	/varset RankArray[3] Tower of Vie III
	
	/varcalc NumClerics ${NumClerics}-1
	/varset ClericString ${ClericString.Replace[|,,]}
	
	/if (${Me.Buff[Tower of Vie].ID}) {
		/return
	}	
	
	/for VieIndex 3 downto 2 {
		/for ClericIndex 1 to ${NumClerics} {
			/if (${Plugin[MQ2DanNet].Version}) {
				/call CastRankOfVieDanNet "${RankArray[${VieIndex}]}" "${ClericString.Arg[${ClericIndex},,]}"
			} else /if (${Plugin[MQ2EQBC].Version}) {
				/call CastRankOfVieEqBc "${RankArray[${VieIndex}]}" "${ClericString.Arg[${ClericIndex},,]}"
			} else {
				/docommand /${AnnounceChannel} EQBC/DanNet not loaded.
				/return
			}
			
			/if (${Macro.Return}) {
				/return
			}
			/next ClericIndex
		}			
		/next VieIndex
	}
	
/return


Sub CastRankOfVieDanNet(string SpellName, string ClericName)
	/dquery ${ClericName} -q "Cast.Ready[${SpellName}]"
	/if (${DanNet.Q}) {
		/squelch /dex ${ClericName} /tar id ${Me.ID}
		/delay 3		
		/squelch /dex ${ClericName} /docommand /${AnnounceChannel} ${SpellName} on - ${Me.Name}
		/squelch /dex ${ClericName} /casting "${SpellName}"
		/delay 6
		/return 1
	}
/return 0


Sub CastRankOfVieEqBc(string SpellName, string ClericName)
	
	/squelch /bct ${ClericName} //if ($\{Cast.Ready[${SpellName}]}) /docommand /${AnnounceChannel} ${SpellName} on - ${Me.Name}
	/squelch /bct ${ClericName} //if ($\{Cast.Ready[${SpellName}]}) /tar id ${Me.ID}
	/squelch /bct ${ClericName} //if ($\{Cast.Ready[${SpellName}]}) /casting "${SpellName}"

	/delay 6
	/if (${Me.Buff[Tower of Vie].ID}) {
		/return 1
	}

/return 0