#warning


|====== Created by Clyde for EZ Server
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.

#warning
#Event cMain	"cMain #1#"

#Event cFindItem "cMain finditem #1#"


|==== Sub Event_cMain
	Sub Event_cMain(Line, cCommand)
		/call cMainLogic ${cCommand}
	/return
|==== End Sub Event_cMain


Sub cMainLogic(cCommand)

	/if (${cCommand.Equal[cViewport]}) {
	
		/squelch /viewport reset
		/squelch ${BroadcastToAllOthers}viewport 0 0 1 1
		
	} else /if (${cCommand.Equal[cFollow]}) {
		/squelch ${BroadcastToAllOthersInZone}tar id ${Me.ID}
		/delay 2
		/squelch ${BroadcastToAllOthersInZone}stick 15 !front uw hold moveback
		/squelch ${BroadCastToAllOthersInZoneSelf}attack off
		
	} else /if (${cCommand.Equal[cStop]}) {
	
		/squelch ${BroadcastToAllOthersInZone}stick off
		/squelch ${BroadcastToAllOthersInZone}attack off
		
	} else /if (${cCommand.Equal[cAssist]}) {
	
		/squelch ${BroadcastToAllOthersInZone}tar id ${Target.ID}
		/delay 2
		/squelch ${BroadcastToAllOthersInZone}stick 15 !front uw hold loose
		/delay 2
		/squelch ${BroadCastToAllOthersInZoneSelf}attack on
		
	} else /if (${cCommand.Equal[cPet]}) {
	
		/squelch /tar id ${Me.Pet.ID}
		/delay 2
		/squelch /say arm yourself
		/delay 2
		/squelch /say arm yourself
		/delay 2
		/squelch /say buff me
		
	} else /if (${cCommand.Equal[cAllHail]}) {
	
		/squelch ${BroadcastToAllOthersInZone}tar id ${Target.ID}
		/delay 2
		/squelch ${BroadcastToAllOthersInZone}hail
		/hail
		
	} else /if (${cCommand.Equal[cHealMe]}) {
	
		/if (${Plugin[MQ2DanNet].Version}) {
			/squelch /dge healer /tar id ${Me.ID}
			/delay 2
			/squelch /dge healer /keypress 0
		} else /if (${Plugin[MQ2EQBC].Version}) {
			/squelch /bct healer //tar id ${Me.ID}
			/delay 2
			/squelch /bct healer //keypress 0
		}
		
	} else /if (${cCommand.Equal[cHealTarget]}) {
	
		/if (${Plugin[MQ2DanNet].Version}) {
			/squelch /dge healer /tar id ${Target.ID}
			/delay 2
			/squelch /dge healer /keypress 0
		} else /if (${Plugin[MQ2EQBC].Version}) {
			/squelch /bct healer //tar id ${Target.ID}
			/delay 2
			/squelch /bct healer //keypress 0
		}
		
	} else /if (${cCommand.Equal[cPull]}) {
		/if (!${Defined[numNpcsNear]}) /declare numNpcsNear int local
	
		/if (!${PullToggle_M}) {
			/varset PullToggle_M 1
			/echo Pull: \agON
		} else {
			/varset PullToggle_M 0
			/echo Pull: \arOFF
		}
		
		/while (${PullToggle_M}) {
			/doevents
			/doevents
			/varset numNpcsNear ${SpawnCount[noalert 2 los npc targetable radius 200]}
			/doevents
			/doevents
			/delay 2
			/squelch /tar id ${NearestSpawn[${Math.Rand[1,${numNpcsNear}]},noalert 2 los npc targetable radius 200].ID}
			/doevents
			/doevents
			/if (${FindItem[Sceptre of Time I].ID}) /itemnotify  "Sceptre of Time I" rightmouseup
			/doevents
			/doevents
			/if (${FindItem[Warrior Class Augment 3.0].ID}) /itemnotify "Warrior Class Augment 3.0" rightmouseup
			/doevents
			/doevents
			/if (${FindItem[Time Rift Power Generator].ID}) /itemnotify "Time Rift Power Generator" rightmouseup
			/doevents
			/doevents
		}
		/doevents
	} else /if (${cCommand.Equal[cPullS]}) {
		/if (${FindItem[Sceptre of Time I].ID}) /itemnotify  "Sceptre of Time I" rightmouseup
		/if (${FindItem[Warrior Class Augment 3.0].ID}) /itemnotify "Warrior Class Augment 3.0" rightmouseup
		/if (${FindItem[Time Rift Power Generator].ID}) /itemnotify "Time Rift Power Generator" rightmouseup
	} else /if (${cCommand.Equal[cUG]}) {
	
		| Will take an items from Pack 9 slot1/2 and combine them in pack 10. Saves some clicks if you get the rhythm down, upgrading a whole toon can be faster.
		/if (!${Window[pack10].Open}) /itemnotify pack10 rightmouseup
		/delay 1m ${Window[pack10].Open}
		/itemnotify in pack9 1 leftmouseup
		/delay 1m ${Cursor.ID}
		/itemnotify in pack10 1 leftmouseup
		/delay 1m !${Cursor.ID}
		/itemnotify in pack9 2 leftmouseup
		/delay 1m ${Cursor.ID}
		/itemnotify in pack10 2 leftmouseup
		/delay 1m !${Cursor.ID}
		/combine pack10
		/autoi		
		/delay 1m !${Cursor.ID}
		/if (${Window[pack10].Open}) /itemnotify pack10 rightmouseup
		
	} else /if (${cCommand.Equal[cYes]}) {
	
		/yes
		
	} else /if (${cCommand.Equal[cCharm]}) {
	
		/itemnotify in pack10 1 leftmouseup
		/delay 1m !${Cursor.ID}
		/ctrl /itemnotify in pack9 1 leftmouseup
		/delay 1m ${Cursor.ID}
		/itemnotify in pack10 2 leftmouseup
		/delay 1m !${Cursor.ID}
		/combine pack10
		/delay 1m ${Cursor.ID}
		
	} else /if (${cCommand.Equal[Gobbie]}) {
	
		/if (${FindItem[Insane Goblin Familiar (Halloween Reward)].ID}) {
			/removebuff Angryface Blessing
			/delay 2
			/itemnotify "Insane Goblin Familiar (Halloween Reward)" rightmouseup
			/delay 5
			/pet leave
		}
		
	} else /if (${cCommand.Equal[AngryFace]}) {
	
		/if (${FindItem[Angryface Familiar (Halloween Reward)].ID}) {
			/removebuff Insane Goblin
			/delay 2
			/itemnotify "Angryface Familiar (Halloween Reward)" rightmouseup
			/delay 5
			/pet leave
		}
		
	} else /if (${cCommand.Equal[SpamKill]}) {
		
		
		/if (${Spawn[npc The Rainkeeper].ID}) {
			/docommand /noparse ${BroadCastToAllOthersInZoneSelf}target  npc The Rainkeeper
		} else /if (${Spawn[npc The Prime Healer].ID}) {
			/docommand /noparse ${BroadCastToAllOthersInZoneSelf}target  npc The Prime Healer
		} else /if (${Spawn[npc The Plaguebringer].ID}) {
			/docommand /noparse ${BroadCastToAllOthersInZoneSelf}target  npc The Plaguebringer
		} else /if (${Spawn[npc The Warlord].ID}) {
			/docommand /noparse ${BroadCastToAllOthersInZoneSelf}target  npc The Warlord
		} else /if (${Spawn[npc The Prince of Hate].ID}) {
			/docommand /noparse ${BroadCastToAllOthersInZoneSelf}target  npc The Prince of Hate
		} else /if (${Spawn[npc The Burning Prince].ID}) {
			/docommand /noparse ${BroadCastToAllOthersInZoneSelf}target  npc The Burning Prince
		} else /if (${Spawn[npc The Mother of All].ID}) {
			/docommand /noparse ${BroadCastToAllOthersInZoneSelf}target  npc The Mother of All
		} else {
			/docommand /noparse ${BroadCastToAllOthersInZoneSelf}target ${NearestSpawn[1, NPC Radius 40]}
		}
		
		/squelch ${BroadcastToAllOthersInZone}echo cSpam
		/casting Bashful Crustaceans' Angerbomb
		/squelch ${BroadCastToAllOthersInZoneSelf}attack on	
		
		
	} else /if (${cCommand.Equal[Illu]}) {
		/multiline ; /tar ${Me.Name} ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar Erely ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar ClydeShaman ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar Jeshetiq ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar "=ClydeBard" ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar "=ClydePally" ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar ClydeRanger ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar ClydeBerToo ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar ClydeDruid ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar ClydeRogue ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar "=ClydeBardToo" ; /casting "Amulet of Necropotence" ; /delay 5
		/multiline ; /tar "=ClydePalToo" ; /casting "Amulet of Necropotence"
	} else /if (${cCommand.Equal[BuffMe]}) {
		/dge /tar id ${Me.ID}
		/delay 2
		/dge /itemnotify 6 rightmouseup
	} else /if (${cCommand.Equal[ProgLoot]}) {
		|/dge ProgChain 		/echo cTurboLoot hh
		|/dge ProgLeather 	/echo cTurboLoot hh
		|/dge ProgPlate 		/echo cTurboLoot hh
		|/dge ProgCloth 		/echo cTurboLoot hh
		|/dge prog			/echo cTurboLoot ${Zone.ShortName}_${Me.Class.ShortName}
		/dge prog			/noparse /echo cTurboLoot T3_${Me.Name}
	} else /if (${cCommand.Equal[cProgGear]}) {
		| Before running, Arrange old tier gear in Pack9, and patterns in Pack8
		| and 7 if you are doing more than 1 tier at a time (T3/4 at the same time etc)
		| Note if you're doing multiple tiers at once, put the lower
		| tier in bag 8 or 9 with the later being in bag 7
		| OPEN ALL BAGS FIRST!
		/declare cProgGearIndex int local 1
		
		/for cProgGearIndex 1 to 7 {
			/if (!${InvSlot[pack9].Item.Item[${cProgGearIndex}].ID}) {
				/next cProgGearIndex
			}
			
			/itemnotify in pack9 ${cProgGearIndex} leftmouseup
			/delay 1m ${Cursor.ID}
			/ctrl /itemnotify in pack10 1 leftmouseup
			/delay 1m !${Cursor.ID}
			/itemnotify in pack8 ${cProgGearIndex} leftmouseup
			/delay 1m ${Cursor.ID}
			/ctrl /itemnotify in pack10 2 leftmouseup
			/delay 1m !${Cursor.ID}
			/combine pack10
			/delay 1m ${Cursor.ID}
			|/if (${InvSlot[pack7].Item.Item[${cProgGearIndex}].ID}) {
			|	/ctrl /itemnotify in pack10 1 leftmouseup
			|	/delay 1m !${Cursor.ID}
			|	/itemnotify in pack7 ${cProgGearIndex} leftmouseup
			|	/delay 1m ${Cursor.ID}
			|	/ctrl /itemnotify in pack10 2 leftmouseup
			|	/delay 1m !${Cursor.ID}
			|	/combine pack10
			|	/delay 1m ${Cursor.ID}
			|}
			/delay 1
			/autoi
			/delay 1
			/delay 1m !${Cursor.ID}
			/next cProgGearIndex
		}		
	} else /if (${cCommand.Equal[LootCorpse]}) {
		/tar corpse ${Me.Name}
		/delay 5
		/nav target
		/loot
		/delay 5
		/keypress esc
	}
	
/return

|----------


|-----------------------------------------------------------------------------------------------
|  SUB: Event_FindItem
|-----------------------------------------------------------------------------------------------
	Sub Event_cFindItem(Line, whichItem)
	
	/if (${Plugin[MQ2EQBC].Version}) {
		/docommand ${If[${FindItemCount[${whichItem}]} > 0,/bc ${FindItemCount[${whichItem}]} ${FindItem[${whichItem}]} in inventory,/echo None in Inventory]}
		/docommand ${If[${FindItemBankCount[${whichItem}]},/bc ${FindItemBankCount[${whichItem}]} ${FindItemBank[${whichItem}]} in bank,/echo None in Bank]}
	}
	/if (${Plugin[MQ2DanNet].Version}) {
		/docommand ${If[${FindItemCount[${whichItem}]} > 0,/dgtell all ${FindItemCount[${whichItem}]} ${FindItem[${whichItem}]} in inventory,/echo None in Inventory]}
		/docommand ${If[${FindItemBankCount[${whichItem}]},/dgtell all ${FindItemBankCount[${whichItem}]} ${FindItemBank[${whichItem}]} in bank,/echo None in Bank]}
	}
	/return